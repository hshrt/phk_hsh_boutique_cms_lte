<?php
require("../../config.php");

ini_set("display_errors", true);

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$movieId = null;

if (isset($_REQUEST['movieId'])) {
    $movieId = $_REQUEST['movieId'];
}

$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

if (isset($_REQUEST['onlyavailable'])) {

    $sql = "SELECT boutique_inventory.id As id, boutique_inventory.stockId As assetId
                FROM boutique_inventory
                WHERE boutique_inventory.isVoid = 0 
                      AND boutique_inventory.available = 1 
                      AND boutique_inventory.productId = '" . $movieId . "' ORDER BY boutique_inventory.stockId ASC";

    $st = $conn->prepare($sql);

    $st->execute();

    $list = array();

    while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
        $list[] = $row;
    }

    $conn = null;

    if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
        echo returnStatus(1, 'get onlyavailable inventoryList good', $list);
    } else {
        echo returnStatus(0, 'get onlyavailable inventoryList fail');
    }

} else {

    $sql = "SELECT boutique_inventory.id As id, boutique_inventory.available as available, boutique_inventory.stockId As assetId, 
            (CASE WHEN hist.roomId IS NULL THEN '-' 
                  WHEN boutique_inventory.available = 1 THEN '-'
                  ELSE hist.roomId END) As roomId, 
                  hist.requestTime As requestTime, 
                (CASE WHEN boutique_inventory.available = 1 THEN 0 ELSE hist.statusId END) As statusId
                FROM boutique_inventory
                
                LEFT JOIN (SELECT 
                            boutique_order_history.inventoryId As inventoryId,
                            boutique_order_history.roomId As roomId,
                            boutique_order_history.requestTime As requestTime,
                            boutique_order_history.statusId As statusId
                          FROM boutique_order_history 
                          WHERE boutique_order_history.statusId = 1 OR boutique_order_history.statusId = 2 OR boutique_order_history.statusId = 3 OR boutique_order_history.statusId = 4) hist
                ON hist.inventoryId = boutique_inventory.id
                
                WHERE boutique_inventory.isVoid = 0 
                      AND boutique_inventory.productId = '" . $movieId . "' ORDER BY boutique_inventory.stockId ASC; ";

    $st = $conn->prepare($sql);

    $st->execute();

    $list = array();

    while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
        $list[] = $row;
    }

    if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
        echo returnStatus(1, 'get inventoryList good', $list);
    } else {
        echo returnStatus(0, 'get inventoryList fail');
    }
    $conn = null;
}

?>
