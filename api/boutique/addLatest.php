<?php

ini_set("display_errors", true);
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$productId = isset($_POST['movieId']) ? $_POST['movieId'] : '';

$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");


$sql = "SELECT MAX(ordering) AS maxOrder FROM boutique_latest ";
$st = $conn->prepare($sql);
$st->execute();


$list = array();
while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $list[] = $row;
}
$maxOrder = $list[0]["maxOrder"];
$maxOrder = $maxOrder + 1;

$sql = "INSERT INTO boutique_latest (ordering, productId, lastUpdate, lastUpdateBy) VALUES (:ordering,:productId,now(),:lastUpdateBy)";
$st = $conn->prepare($sql);
$st->bindValue(":ordering", $maxOrder, PDO::PARAM_INT);
$st->bindValue(":productId", $productId, PDO::PARAM_STR);
$st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
$st->execute();

if ($st->rowCount() > 0) {
    echo returnStatus(1, 'Add latest OK');
} else {
    echo returnStatus(0, 'Add latest fail');
}

return 0;

?>
