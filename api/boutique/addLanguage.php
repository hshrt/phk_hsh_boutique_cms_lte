<?php

ini_set("display_errors", true);
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$title_zh_cn = isset($_POST['title_zh_cn']) ? $_POST['title_zh_cn'] : '';
$title_en = isset($_POST['title_en']) ? $_POST['title_en'] : '';
$title_zh_hk = isset($_POST['title_zh_hk']) ? $_POST['title_zh_hk'] : '';
$title_jp = isset($_POST['title_jp']) ? $_POST['title_jp'] : '';
$title_fr = isset($_POST['title_fr']) ? $_POST['title_fr'] : '';
$title_ar = isset($_POST['title_ar']) ? $_POST['title_ar'] : '';
$title_es = isset($_POST['title_es']) ? $_POST['title_es'] : '';
$title_de = isset($_POST['title_de']) ? $_POST['title_de'] : '';
$title_ko = isset($_POST['title_ko']) ? $_POST['title_ko'] : '';
$title_ru = isset($_POST['title_ru']) ? $_POST['title_ru'] : '';
$title_pt = isset($_POST['title_pt']) ? $_POST['title_pt'] : '';

if (empty($title_zh_cn)) {
    echo returnStatus(0, 'at least input zh_cn');
} else {
    $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
    $conn->exec("set names utf8");

    //*****create Dictionary for Subject
    $sql = "SELECT UUID() AS UUID";
    $st = $conn->prepare($sql);
    $st->execute();

    $list = array();

    while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
        $list[] = $row;
    }
    $uuid = $list[0]["UUID"];

    $sql = "INSERT INTO movie_dictionary (id,en,zh_hk,zh_cn,jp,fr,ar,es,de,ko,ru,pt,type,lastUpdate, lastUpdateBy) VALUES (:id,
:en, :zh_hk, :zh_cn, :jp, :fr, :ar, :es, :de,:ko, :ru,:pt,:type,now(),:lastUpdateBy)";
    $st = $conn->prepare($sql);
    $st->bindValue(":id", $uuid, PDO::PARAM_STR);
    $st->bindValue(":en", $title_en, PDO::PARAM_STR);
    $st->bindValue(":zh_hk", $title_zh_hk, PDO::PARAM_STR);
    $st->bindValue(":zh_cn", $title_zh_cn, PDO::PARAM_STR);
    $st->bindValue(":jp", $title_jp, PDO::PARAM_STR);
    $st->bindValue(":fr", $title_fr, PDO::PARAM_STR);
    $st->bindValue(":ar", $title_ar, PDO::PARAM_STR);
    $st->bindValue(":es", $title_es, PDO::PARAM_STR);
    $st->bindValue(":de", $title_de, PDO::PARAM_STR);
    $st->bindValue(":ko", $title_ko, PDO::PARAM_STR);
    $st->bindValue(":ru", $title_ru, PDO::PARAM_STR);
    $st->bindValue(":pt", $title_pt, PDO::PARAM_STR);
    $st->bindValue(":type", "lang", PDO::PARAM_STR);
    $st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
    $st->execute();
    $titleId = $uuid;

    if ($st->rowCount() > 0) {
        $sql = "INSERT INTO movie_language (titleId, lastUpdate, lastUpdateBy) VALUES (:titleId,now(),:lastUpdateBy)";
        $st = $conn->prepare($sql);
        $st->bindValue(":titleId", $titleId, PDO::PARAM_STR);
        $st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
        $st->execute();

        if ($st->rowCount() > 0) {
            echo returnStatus(1, 'Add language OK');
        } else {
            echo returnStatus(0, 'Add language fail');
        }

    } else {
        echo returnStatus(0, 'Add language fail');
    }

}
return 0;

?>
