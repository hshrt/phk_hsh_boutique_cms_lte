<?php

ini_set("display_errors", true);

require("../../config.php");
require("../../php/func_nx.php");
require("../../php/inc.appvars.php");

session_start();

$requestId = isset($_POST['requestId']) ? $_POST['requestId'] : null;
$statusId = isset($_POST['statusId']) ? $_POST['statusId'] : null;
$inventoryId = isset($_POST['inventoryId']) ? $_POST['inventoryId'] : null;
$updateBy =  ($_SESSION['email'] != null) ?  $_SESSION['email'] : "bedside";
$stckId = isset($_POST['stckId']) ? $_POST['stckId'] : null;
$prodId = isset($_POST['prodId']) ? $_POST['prodId'] : null;

if ($requestId == null || $statusId == null) {
    echo returnStatus(1, 'update boutique fail');
} else {

    //setup DB
    $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
    $conn->exec("set names utf8");

    if ($inventoryId != null) {
        $sql = "UPDATE boutique_order_history SET inventoryId=:inventoryId, statusId=:statusId, lastUpdate=now(), lastUpdateBy=:lastUpdateBy WHERE id = " . $requestId . " ";
    } else {
        $sql = "UPDATE boutique_order_history SET statusId=:statusId, lastUpdate=now(), lastUpdateBy=:lastUpdateBy WHERE id = " . $requestId . " ";
    }

    //echo $sql;
    $st = $conn->prepare($sql);

    if ($inventoryId != null) {
        $st->bindValue(":inventoryId", $inventoryId, PDO::PARAM_STR);
    }

    $st->bindValue(":statusId", $statusId, PDO::PARAM_STR);
    $st->bindValue(":lastUpdateBy", $updateBy, PDO::PARAM_STR);



    $st->execute();

    if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {

        if($statusId == 5 || $statusId == 6 || $statusId == 16) {

            $sql = "SELECT id as id from boutique_inventory where reqId = ".$requestId." ";
            $st1 = $conn->prepare($sql);
            $st1->execute();

            $list = array();
            while ($row = $st1->fetch(PDO::FETCH_ASSOC)) {
                $list[] = $row;
				$curId = $row["id"];
				
				$sql = "UPDATE boutique_inventory SET available=1, lastUpdate=now(), lastUpdateBy=:lastUpdateBy, reqId=:reqId WHERE id = '".$curId."'";
			//	$kart = $kart - 1;
				$st = $conn->prepare($sql);
				$st->bindValue(":reqId", null, PDO::PARAM_INT);
				$st->bindValue(":lastUpdateBy", $updateBy, PDO::PARAM_STR);
				$st->execute();
			}

            if ($st1->fetchColumn() > 0 || $st1->rowCount() > 0) {
                echo returnStatus(1, 'update request good');
            } else {
                echo returnStatus(0, 'update request fail');
            }
        } else {
            echo returnStatus(1, 'update request good');
        }

    } else{
        echo returnStatus(0, 'update request fail');
    }

    $conn = null;
}

?>
