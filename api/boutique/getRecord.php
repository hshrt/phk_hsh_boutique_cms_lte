<?php
require("../../config.php");

ini_set("display_errors", true);

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

$movieId = null;
$room = null;
$lang = "en";
$sqlForFilter = '';

if (isset($_REQUEST["movieId"]) && $_REQUEST["movieId"] != null && strlen($_REQUEST["movieId"]) > 1) {
    $movieId = $_REQUEST["movieId"];
    $sqlForFilter = $sqlForFilter . " AND moviedetail.movieId = '" . $movieId . "' ";
}

if (isset($_REQUEST["room"]) && $_REQUEST["room"] != null && strlen($_REQUEST["room"]) > 1) {
    $room = $_REQUEST["room"];
    $sqlForFilter .= $sqlForFilter . " AND hist.roomId = '" . $room . "' ";
}

if (isset($_REQUEST["lang"])) {
    $lang = $_REQUEST["lang"];
}

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "SELECT 
                hist.id As id,
                hist.roomId As room,
                inventory.assetId As assetId,
                moviedetail.movieId As productId,
                moviedetail.movieTitle As title,
                moviedetail.poster As poster,
                moviedetail.year As year,
                hist.statusId As statusId,
				hist.quantity As quantity, 
				hist.reqDeliveryTime As deliveryTime, 
                hist.requestTime As requesttime
        
            FROM boutique_order_history hist 
            
            LEFT JOIN 
                (SELECT boutique.id As movieId, 
                (CASE boutique_dictionary." . $lang . " WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary." . $lang . " END ) AS movieTitle,
                boutique.price As year,
                boutique.posterurl As poster
                 FROM boutique 
                 INNER JOIN boutique_dictionary
                 ON boutique.titleId = boutique_dictionary.id) moviedetail
            ON moviedetail.movieId = hist.productId
            
            LEFT JOIN (SELECT boutique_inventory.id As inventoryId, boutique_inventory.stockId As assetId 
                        FROM boutique_inventory) inventory
            ON inventory.inventoryId = hist.inventoryId 
            
            WHERE enable = 1 AND statusId < 10 " . $sqlForFilter . " 
            ORDER BY hist.requestTime DESC;";

$st = $conn->prepare($sql);
$st->execute();

$list = array();

while ($row = $st->fetch(PDO::FETCH_ASSOC)) {

    $currentMovieId = $row["productId"];

    $remarkString = null;
    $sql = "SELECT DISTINCT boutique.remark As remark 
                  FROM boutique
                WHERE boutique.id = '" . $currentMovieId . "' ";

    $st2 = $conn->prepare($sql);
    $st2->execute();



    while ($row2 = $st2->fetch(PDO::FETCH_ASSOC)) {
        if ($remarkString == null) {
            $remarkString = $row2["remark"];
        } else {
            $remarkString = $remark . "," . $row2["remark"];
        }

    }
    $row["remark"] = $remarkString;

    $list[] = $row;
}

$conn = null;

if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'get record good', $list);
} else {
    echo returnStatus(0, 'get record fail');
}
?>
