<?php

require("../../config.php");
require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();
include("../checkSession.php");

$productId = $_POST["id"];

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");


$sql = "UPDATE boutique SET isVoid=1, lastUpdate=now(),lastUpdateBy=:lastUpdateBy WHERE id = :productId;";

//echo $sql;

$st = $conn->prepare($sql);
$st->bindValue(":productId", $productId, PDO::PARAM_STR);
$st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);

$st->execute();


$sql = "DELETE FROM boutiques_category WHERE productId = '" .$productId."'" ;
$st = $conn->prepare($sql);
$st->execute();

$conn = null;

if($st->fetchColumn() > 0 || $st->rowCount() > 0){
    echo returnStatus(1 , 'delete product good');
}
else{
    echo returnStatus(0 , 'delete product fail');
}

?>
