<?php

ini_set("display_errors", true);

require("../../config.php");
require("../../php/func_nx.php");
require("../../php/inc.appvars.php");

session_start();

//setup DB
$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "SELECT boutique.posterurl as posterUrl, boutique.posterurl1 as posterUrl1, boutique.posterurl2 as posterUrl2, boutique.posterurl3 as posterUrl3 from boutique WHERE isVoid = 1;";
$st = $conn->prepare($sql);
$st->execute();

while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $posterUrl = $row["posterUrl"];
	$posterUrl1 = $row["posterUrl1"];
	$posterUrl2 = $row["posterUrl2"];
	$posterUrl3 = $row["posterUrl3"];
	if (!empty($posterUrl))
		$local_file_path = '../../'.$posterUrl;
	if (!empty($posterUrl1))
		$local_file_path1 = '../../'.$posterUrl1;
	if (!empty($posterUrl2))
		$local_file_path2 = '../../'.$posterUrl2;
	if (!empty($posterUrl3))
		$local_file_path3 = '../../'.$posterUrl3;
	if (!empty($posterUrl) && file_exists($local_file_path))
		unlink($local_file_path);
	if (!empty($posterUrl1) && file_exists($local_file_path1))
		unlink($local_file_path1);
	if (!empty($posterUrl2) && file_exists($local_file_path2))
		unlink($local_file_path2);
	if (!empty($posterUrl3) && file_exists($local_file_path3))
		unlink($local_file_path3);

}
$sql = "DELETE from boutique WHERE isVoid = 1;";
$st = $conn->prepare($sql);
$st->execute();

if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'delete good');
} else {
    echo returnStatus(0, 'delete fail');
}

$conn = null;

?>
