<?php
require( "../../config.php" );

ini_set( "display_errors", true );

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$statusSunday = isset($_POST['statusSunday']) ? $_POST['statusSunday'] : 'open';
$startTimeSunday = isset($_POST['startTimeSunday']) ? $_POST['startTimeSunday'] : '00:00';
$endTimeSunday = isset($_POST['endTimeSunday']) ? $_POST['endTimeSunday'] : '00:00';
$statusMonday = isset($_POST['statusMonday']) ? $_POST['statusMonday'] : 'open';
$startTimeMonday = isset($_POST['startTimeMonday']) ? $_POST['startTimeMonday'] : '00:00';
$endTimeMonday = isset($_POST['endTimeMonday']) ? $_POST['endTimeMonday'] : '00:00';
$statusTuesday = isset($_POST['statusTuesday']) ? $_POST['statusTuesday'] : 'open';
$startTimeTuesday = isset($_POST['startTimeTuesday']) ? $_POST['startTimeTuesday'] : '00:00';
$endTimeTuesday = isset($_POST['endTimeTuesday']) ? $_POST['endTimeTuesday'] : '00:00';
$statusWednesday = isset($_POST['statusWednesday']) ? $_POST['statusWednesday'] : 'open';
$startTimeWednesday = isset($_POST['startTimeWednesday']) ? $_POST['startTimeWednesday'] : '00:00';
$endTimeWednesday = isset($_POST['endTimeWednesday']) ? $_POST['endTimeWednesday'] : '00:00';
$statusThursday = isset($_POST['statusThursday']) ? $_POST['statusThursday'] : 'open';
$startTimeThursday = isset($_POST['startTimeThursday']) ? $_POST['startTimeThursday'] : '00:00';
$endTimeThursday = isset($_POST['endTimeThursday']) ? $_POST['endTimeThursday'] : '00:00';
$statusFriday = isset($_POST['statusFriday']) ? $_POST['statusFriday'] : 'open';
$startTimeFriday = isset($_POST['startTimeFriday']) ? $_POST['startTimeFriday'] : '00:00';
$endTimeFriday = isset($_POST['endTimeFriday']) ? $_POST['endTimeFriday'] : '00:00';
$statusSaturday = isset($_POST['statusSaturday']) ? $_POST['statusSaturday'] : 'open';
$startTimeSaturday = isset($_POST['startTimeSaturday']) ? $_POST['startTimeSaturday'] : '00:00';
$endTimeSaturday = isset($_POST['endTimeSaturday']) ? $_POST['endTimeSaturday'] : '00:00';

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "UPDATE boutique_openinghours SET status = :status , 
        startTime = :startTime, endTime = :endTime where day = :day";

$st = $conn->prepare($sql);

$st->bindValue(":status", $statusSunday, PDO::PARAM_STR);
$st->bindValue(":startTime", $startTimeSunday, PDO::PARAM_STR);
$st->bindValue(":endTime", $endTimeSunday, PDO::PARAM_STR);
$st->bindValue(":day", "Sunday", PDO::PARAM_STR);

$st->execute();

$sql = "UPDATE boutique_openinghours SET status = :status , 
        startTime = :startTime, endTime = :endTime where day = :day";

$st = $conn->prepare($sql);

$st->bindValue(":status", $statusMonday, PDO::PARAM_STR);
$st->bindValue(":startTime", $startTimeMonday, PDO::PARAM_STR);
$st->bindValue(":endTime", $endTimeMonday, PDO::PARAM_STR);
$st->bindValue(":day", "Monday", PDO::PARAM_STR);

$st->execute();

$sql = "UPDATE boutique_openinghours SET status = :status , 
        startTime = :startTime, endTime = :endTime where day = :day";

$st = $conn->prepare($sql);

$st->bindValue(":status", $statusTuesday, PDO::PARAM_STR);
$st->bindValue(":startTime", $startTimeTuesday, PDO::PARAM_STR);
$st->bindValue(":endTime", $endTimeTuesday, PDO::PARAM_STR);
$st->bindValue(":day", "Tuesday", PDO::PARAM_STR);

$st->execute();

$sql = "UPDATE boutique_openinghours SET status = :status , 
        startTime = :startTime, endTime = :endTime where day = :day";

$st = $conn->prepare($sql);

$st->bindValue(":status", $statusWednesday, PDO::PARAM_STR);
$st->bindValue(":startTime", $startTimeWednesday, PDO::PARAM_STR);
$st->bindValue(":endTime", $endTimeWednesday, PDO::PARAM_STR);
$st->bindValue(":day", "Wednesday", PDO::PARAM_STR);

$st->execute();

$sql = "UPDATE boutique_openinghours SET status = :status , 
        startTime = :startTime, endTime = :endTime where day = :day";

$st = $conn->prepare($sql);

$st->bindValue(":status", $statusThursday, PDO::PARAM_STR);
$st->bindValue(":startTime", $startTimeThursday, PDO::PARAM_STR);
$st->bindValue(":endTime", $endTimeThursday, PDO::PARAM_STR);
$st->bindValue(":day", "Thursday", PDO::PARAM_STR);

$st->execute();

$sql = "UPDATE boutique_openinghours SET status = :status , 
        startTime = :startTime, endTime = :endTime where day = :day";

$st = $conn->prepare($sql);

$st->bindValue(":status", $statusFriday, PDO::PARAM_STR);
$st->bindValue(":startTime", $startTimeFriday, PDO::PARAM_STR);
$st->bindValue(":endTime", $endTimeFriday, PDO::PARAM_STR);
$st->bindValue(":day", "Friday", PDO::PARAM_STR);

$st->execute();

$sql = "UPDATE boutique_openinghours SET status = :status , 
        startTime = :startTime, endTime = :endTime where day = :day";

$st = $conn->prepare($sql);

$st->bindValue(":status", $statusSaturday, PDO::PARAM_STR);
$st->bindValue(":startTime", $startTimeSaturday, PDO::PARAM_STR);
$st->bindValue(":endTime", $endTimeSaturday, PDO::PARAM_STR);
$st->bindValue(":day", "Saturday", PDO::PARAM_STR);

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$conn = null;

if($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'get update good', $list);
}
else{
    echo returnStatus(0, 'get update fail');
}

?>