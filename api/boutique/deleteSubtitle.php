<?php

require("../../config.php");
require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();
include("../checkSession.php");

$id = $_POST["id"];

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$delete_id_array = array();

//push the id of the delete item into the array
array_push($delete_id_array,$id);
$delete_ids_string = implode(",",$delete_id_array);

//echo($delete_ids_string);

$sql = "DELETE movie_subtitle,
       movie_dictionary
FROM movie_subtitle,
     movie_dictionary
WHERE movie_subtitle.titleId = movie_dictionary.id  AND movie_subtitle.id IN ($delete_ids_string);";

$st = $conn->prepare ( $sql );
$st->bindValue( ":id", $delete_ids_string, PDO::PARAM_STR);
$st->execute();

$conn = null;

if($st->fetchColumn() > 0 || $st->rowCount() > 0){
    echo returnStatus(1 , 'delete subtitle good');
}
else{
    echo returnStatus(0 , 'delete subtitle fail');
}

?>
