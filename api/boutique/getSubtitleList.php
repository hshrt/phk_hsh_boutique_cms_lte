<?php
require( "../../config.php" );

ini_set( "display_errors", true );

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$page = 0;
$itemPerPage = 15;
$getCount = null;

if(isset($_REQUEST['getCount'])){
    $getCount = $_REQUEST['getCount'];
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

if(!empty($getCount)){
    $sql = "select  count(*) as totalNum from movie_subtitle where movie_subtitle.titleId !='' ";
} else {
    $sql = "SELECT  movie_subtitle.id As id,
                movie_subtitle.titleId As titleId,
                movie_dictionary.en AS en,
                (CASE movie_dictionary.zh_cn WHEN '' THEN movie_dictionary.en ELSE movie_dictionary.zh_cn END ) AS zh_cn,
                (CASE movie_dictionary.fr WHEN '' THEN movie_dictionary.en ELSE movie_dictionary.fr END ) AS fr,
                (CASE movie_dictionary.jp WHEN '' THEN movie_dictionary.en ELSE movie_dictionary.jp END ) AS jp,
                (CASE movie_dictionary.ar WHEN '' THEN movie_dictionary.en ELSE movie_dictionary.ar END ) AS ar,
                (CASE movie_dictionary.es WHEN '' THEN movie_dictionary.en ELSE movie_dictionary.es END ) AS es,
                (CASE movie_dictionary.de WHEN '' THEN movie_dictionary.en ELSE movie_dictionary.de END ) AS de,
                (CASE movie_dictionary.ko WHEN '' THEN movie_dictionary.en ELSE movie_dictionary.ko END ) AS ko,
                (CASE movie_dictionary.ru WHEN '' THEN movie_dictionary.en ELSE movie_dictionary.ru END ) AS ru,
                (CASE movie_dictionary.pt WHEN '' THEN movie_dictionary.en ELSE movie_dictionary.pt END ) AS pt,
                (CASE movie_dictionary.zh_hk WHEN '' THEN movie_dictionary.en ELSE movie_dictionary.zh_hk END ) AS zh_hk,
                (CASE movie_dictionary.tr WHEN '' THEN movie_dictionary.en ELSE movie_dictionary.tr END ) AS tr
            FROM movie_subtitle 
            INNER JOIN movie_dictionary
            ON movie_subtitle.titleId = movie_dictionary.id
            ORDER BY movie_subtitle.id ASC";
}


$st = $conn->prepare($sql);

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$conn = null;

if($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'get itemList good', $list);
}
else{
    echo returnStatus(0, 'get itemList fail');
}

?>
