<?php
require("../../config.php");

ini_set("display_errors", true);

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$genreId = null;
$lang = null;

if (isset($_REQUEST['genreId'])) {
    $genreId = $_REQUEST['genreId'];
}

if (isset($_REQUEST['lang'])) {
    $lang = $_REQUEST['lang'];
} else {
    $lang = 'en';
}

$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "";

if($genreId == null){

    $sql = "SELECT DISTINCT boutique.id As movieId, title.movieTitle As movieTitle, boutique.posterurl As poster
        FROM boutique
        
        INNER JOIN 
            (SELECT DISTINCT boutique.titleId AS titleId, 
            (CASE boutique_dictionary." . $lang . " WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary." . $lang . " END ) AS movieTitle
             FROM boutique 
             INNER JOIN boutique_dictionary
             ON boutique.titleId = boutique_dictionary.id) title
        ON title.titleId = boutique.titleId 
        WHERE boutique.isVoid = 0 ";

        $sql = $sql."GROUP BY boutique.id ORDER BY title.movieTitle ASC;";

} else {

    $sql = "SELECT DISTINCT boutique.id As movieId, title.movieTitle As movieTitle, boutique.posterurl As poster
        FROM boutique
        
        INNER JOIN 
            (SELECT DISTINCT boutique.titleId AS titleId, 
            (CASE boutique_dictionary." . $lang . " WHEN '' THEN boutique_dictionary.en ELSE boutique_dictionary." . $lang . " END ) AS movieTitle
             FROM boutique 
             INNER JOIN boutique_dictionary
             ON boutique.titleId = boutique_dictionary.id) title
        ON title.titleId = boutique.titleId 
        
        INNER JOIN boutiques_category 
        ON boutique.id = boutiques_category.productId
        
        WHERE boutiques_category.categoryId = '".$genreId."' AND boutique.isVoid = 0 ";
        $sql = $sql."GROUP BY boutique.id ORDER BY title.movieTitle ASC;";
}

$st = $conn->prepare($sql);

$st->execute();

$list = array();

while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $list[] = $row;
}

$conn = null;

if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'get productList by genre good', $list);
} else {
    echo returnStatus(0, 'get productList by genre fail', $sql);
}

?>
