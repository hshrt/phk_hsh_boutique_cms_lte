<?php

ini_set( "display_errors", true );
require( "../../config.php" );

require("../../php/inc.appvars.php");

require_once "../../php/func_nx.php";
require_once "../../php/func_json.php";

require("../../php/lib/resize.class.php");

session_start();


$photo_data = $_POST['image'];
$photo_data1 = $_POST['image1'];
$photo_data2 = $_POST['image2'];
$photo_data3 = $_POST['image3'];
$fileExtension = isset($_POST['fileExtension'])?$_POST['fileExtension']:"jpg";

if($fileExtension=="jpeg"){
    $fileExtension = "jpg";
}

$type = isset($_POST['type'])?$_POST['type']:null;
$album = isset($_POST['album'])?$_POST['album']:"";

if (empty($photo_data) && empty($photo_data1) && empty($photo_data2) && empty($photo_data3)) {
    echo returnStatus(0, 'missing_img_data');
    exit;
}else{
    $timestamp = date('m_d_Y_h_i_s', time());
    if (!empty($photo_data) && empty($photo_data1) && empty($photo_data2) && empty($photo_data3)) {
		$file_name = $timestamp . '.' .$fileExtension;
		$file_path = 'upload/'.$file_name;
		$local_file_path = '../../upload/'.$file_name;
		if (file_put_contents($local_file_path, base64_decode($photo_data))) {
			echo returnStatus(1 , 'good', array('filepath' => $file_path));
		} else {
			echo returnStatus(0 , 'error');
		}
	} else if (empty($photo_data) && !empty($photo_data1) && empty($photo_data2) && empty($photo_data3)) {
		$file_name = $timestamp . '_1.' .$fileExtension;
		$file_path = 'upload/'.$file_name;
		$local_file_path = '../../upload/'.$file_name;
		if (file_put_contents($local_file_path, base64_decode($photo_data1))) {
			echo returnStatus(1 , 'good', array('filepath1' => $file_path));
		} else {
			echo returnStatus(0 , 'error');
		}
	} else if (empty($photo_data) && empty($photo_data1) && !empty($photo_data2) && empty($photo_data3)) {
		$file_name = $timestamp . '_2.' .$fileExtension;
		$file_path = 'upload/'.$file_name;
		$local_file_path = '../../upload/'.$file_name;
		if (file_put_contents($local_file_path, base64_decode($photo_data2))) {
			echo returnStatus(1 , 'good', array('filepath2' => $file_path));
		} else {
			echo returnStatus(0 , 'error');
		}
	} else if (empty($photo_data) && empty($photo_data1) && empty($photo_data2) && !empty($photo_data3)) {
		$file_name = $timestamp . '_3.' .$fileExtension;
		$file_path = 'upload/'.$file_name;
		$local_file_path = '../../upload/'.$file_name;
		if (file_put_contents($local_file_path, base64_decode($photo_data3))) {
			echo returnStatus(1 , 'good', array('filepath3' => $file_path));
		} else {
			echo returnStatus(0 , 'error');
		}
	}  else if (!empty($photo_data) && !empty($photo_data1) && empty($photo_data2) && empty($photo_data3)) {
		$file_name = $timestamp . '.' .$fileExtension;
		$file_path = 'upload/'.$file_name;
		$local_file_path = '../../upload/'.$file_name;
		$file_name1 = $timestamp . '_1.' .$fileExtension;
		$file_path1 = 'upload/'.$file_name1;
		$local_file_path1 = '../../upload/'.$file_name1;
		if (file_put_contents($local_file_path, base64_decode($photo_data)) && file_put_contents($local_file_path1, base64_decode($photo_data1))) {
			echo returnStatus(1 , 'good', array('filepath' => $file_path, 'filepath1' => $file_path1));
		} else {
			echo returnStatus(0 , 'error');
		}
	} else if (!empty($photo_data) && empty($photo_data1) && !empty($photo_data2) && empty($photo_data3)) {
		$file_name = $timestamp . '.' .$fileExtension;
		$file_path = 'upload/'.$file_name;
		$local_file_path = '../../upload/'.$file_name;
		$file_name1 = $timestamp . '_1.' .$fileExtension;
		$file_path1 = 'upload/'.$file_name1;
		$local_file_path1 = '../../upload/'.$file_name1;
		if (file_put_contents($local_file_path, base64_decode($photo_data)) && file_put_contents($local_file_path1, base64_decode($photo_data2))) {
			echo returnStatus(1 , 'good', array('filepath' => $file_path, 'filepath2' => $file_path1));
		} else {
			echo returnStatus(0 , 'error');
		}
	} else if (!empty($photo_data) && empty($photo_data1) && empty($photo_data2) && !empty($photo_data3)) {
		$file_name = $timestamp . '.' .$fileExtension;
		$file_path = 'upload/'.$file_name;
		$local_file_path = '../../upload/'.$file_name;
		$file_name1 = $timestamp . '_1.' .$fileExtension;
		$file_path1 = 'upload/'.$file_name1;
		$local_file_path1 = '../../upload/'.$file_name1;
		if (file_put_contents($local_file_path, base64_decode($photo_data)) && file_put_contents($local_file_path1, base64_decode($photo_data3))) {
			echo returnStatus(1 , 'good', array('filepath' => $file_path, 'filepath3' => $file_path1));
		} else {
			echo returnStatus(0 , 'error');
		}
   } else if (empty($photo_data) && !empty($photo_data1) && !empty($photo_data2) && empty($photo_data3)) {
		$file_name = $timestamp . '.' .$fileExtension;
		$file_path = 'upload/'.$file_name;
		$local_file_path = '../../upload/'.$file_name;
		$file_name1 = $timestamp . '_1.' .$fileExtension;
		$file_path1 = 'upload/'.$file_name1;
		$local_file_path1 = '../../upload/'.$file_name1;
		if (file_put_contents($local_file_path, base64_decode($photo_data1)) && file_put_contents($local_file_path1, base64_decode($photo_data2))) {
			echo returnStatus(1 , 'good', array('filepath1' => $file_path, 'filepath2' => $file_path1));
		} else {
			echo returnStatus(0 , 'error');
		}
   } else if (empty($photo_data) && !empty($photo_data1) && empty($photo_data2) && !empty($photo_data3)) {
		$file_name = $timestamp . '.' .$fileExtension;
		$file_path = 'upload/'.$file_name;
		$local_file_path = '../../upload/'.$file_name;
		$file_name1 = $timestamp . '_1.' .$fileExtension;
		$file_path1 = 'upload/'.$file_name1;
		$local_file_path1 = '../../upload/'.$file_name1;
		if (file_put_contents($local_file_path, base64_decode($photo_data1)) && file_put_contents($local_file_path1, base64_decode($photo_data3))) {
			echo returnStatus(1 , 'good', array('filepath1' => $file_path, 'filepath3' => $file_path1));
		} else {
			echo returnStatus(0 , 'error');
		}
   } else if (empty($photo_data) && empty($photo_data1) && !empty($photo_data2) && !empty($photo_data3)) {
		$file_name = $timestamp . '.' .$fileExtension;
		$file_path = 'upload/'.$file_name;
		$local_file_path = '../../upload/'.$file_name;
		$file_name1 = $timestamp . '_1.' .$fileExtension;
		$file_path1 = 'upload/'.$file_name1;
		$local_file_path1 = '../../upload/'.$file_name1;
		if (file_put_contents($local_file_path, base64_decode($photo_data2)) && file_put_contents($local_file_path1, base64_decode($photo_data3))) {
			echo returnStatus(1 , 'good', array('filepath2' => $file_path, 'filepath3' => $file_path1));
		} else {
			echo returnStatus(0 , 'error');
		}
   } else if (!empty($photo_data) && !empty($photo_data1) && !empty($photo_data2) && empty($photo_data3)) {
		$file_name = $timestamp . '.' .$fileExtension;
		$file_path = 'upload/'.$file_name;
		$local_file_path = '../../upload/'.$file_name;
		$file_name1 = $timestamp . '_1.' .$fileExtension;
		$file_path1 = 'upload/'.$file_name1;
		$local_file_path1 = '../../upload/'.$file_name1;
		$file_name2 = $timestamp . '_2.' .$fileExtension;
		$file_path2 = 'upload/'.$file_name2;
		$local_file_path2 = '../../upload/'.$file_name2;		
		if (file_put_contents($local_file_path, base64_decode($photo_data)) && file_put_contents($local_file_path1, base64_decode($photo_data1)) && file_put_contents($local_file_path2, base64_decode($photo_data2))) {
			echo returnStatus(1 , 'good', array('filepath' => $file_path, 'filepath1' => $file_path1, 'filepath2' => $file_path2));
		} else {
			echo returnStatus(0 , 'error');
		}
   } else if (empty($photo_data) && !empty($photo_data1) && !empty($photo_data2) && !empty($photo_data3)) {
		$file_name = $timestamp . '.' .$fileExtension;
		$file_path1 = 'upload/'.$file_name;
		$local_file_path = '../../upload/'.$file_name;
		$file_name1 = $timestamp . '_1.' .$fileExtension;
		$file_path2 = 'upload/'.$file_name1;
		$local_file_path1 = '../../upload/'.$file_name1;
		$file_name2 = $timestamp . '_2.' .$fileExtension;
		$file_path3 = 'upload/'.$file_name2;
		$local_file_path2 = '../../upload/'.$file_name2;		
		if (file_put_contents($local_file_path, base64_decode($photo_data1)) && file_put_contents($local_file_path1, base64_decode($photo_data2)) && file_put_contents($local_file_path2, base64_decode($photo_data3))) {
			echo returnStatus(1 , 'good', array('filepath1' => $file_path1, 'filepath2' => $file_path2, 'filepath3' => $file_path3));
		} else {
			echo returnStatus(0 , 'error');
		}
   } else if (!empty($photo_data) && empty($photo_data1) && !empty($photo_data2) && !empty($photo_data3)) {
		$file_name = $timestamp . '.' .$fileExtension;
		$file_path = 'upload/'.$file_name;
		$local_file_path = '../../upload/'.$file_name;
		$file_name1 = $timestamp . '_1.' .$fileExtension;
		$file_path2 = 'upload/'.$file_name1;
		$local_file_path1 = '../../upload/'.$file_name1;
		$file_name2 = $timestamp . '_2.' .$fileExtension;
		$file_path3 = 'upload/'.$file_name2;
		$local_file_path2 = '../../upload/'.$file_name2;		
		if (file_put_contents($local_file_path, base64_decode($photo_data)) && file_put_contents($local_file_path1, base64_decode($photo_data2)) && file_put_contents($local_file_path2, base64_decode($photo_data3))) {
			echo returnStatus(1 , 'good', array('filepath' => $file_path, 'filepath2' => $file_path2, 'filepath3' => $file_path3));
		} else {
			echo returnStatus(0 , 'error');
		}
   } else if (!empty($photo_data) && !empty($photo_data1) && empty($photo_data2) && !empty($photo_data3)) {
		$file_name = $timestamp . '.' .$fileExtension;
		$file_path = 'upload/'.$file_name;
		$local_file_path = '../../upload/'.$file_name;
		$file_name1 = $timestamp . '_1.' .$fileExtension;
		$file_path1 = 'upload/'.$file_name1;
		$local_file_path1 = '../../upload/'.$file_name1;
		$file_name2 = $timestamp . '_3.' .$fileExtension;
		$file_path3 = 'upload/'.$file_name2;
		$local_file_path2 = '../../upload/'.$file_name2;		
		if (file_put_contents($local_file_path, base64_decode($photo_data)) && file_put_contents($local_file_path1, base64_decode($photo_data1)) && file_put_contents($local_file_path2, base64_decode($photo_data3))) {
			echo returnStatus(1 , 'good', array('filepath' => $file_path, 'filepath1' => $file_path1, 'filepath3' => $file_path3));
		} else {
			echo returnStatus(0 , 'error');
		}
   } else {
		$file_name = $timestamp . '.' .$fileExtension;
		$file_path = 'upload/'.$file_name;
		$local_file_path = '../../upload/'.$file_name;
		$file_name1 = $timestamp . '_1.' .$fileExtension;
		$file_path1 = 'upload/'.$file_name1;
		$local_file_path1 = '../../upload/'.$file_name1;
		$file_name2 = $timestamp . '_2.' .$fileExtension;
		$file_path2 = 'upload/'.$file_name2;
		$local_file_path2 = '../../upload/'.$file_name2;
		$file_name3 = $timestamp . '_3.' .$fileExtension;
		$file_path3 = 'upload/'.$file_name3;
		$local_file_path3 = '../../upload/'.$file_name3;	
		if (file_put_contents($local_file_path, base64_decode($photo_data)) && file_put_contents($local_file_path1, base64_decode($photo_data1)) && file_put_contents($local_file_path2, base64_decode($photo_data2)) && file_put_contents($local_file_path3, base64_decode($photo_data3))) {
			echo returnStatus(1 , 'good', array('filepath' => $file_path, 'filepath1' => $file_path1, 'filepath2' => $file_path2,'filepath3' => $file_path3));
		} else {
			echo returnStatus(0 , 'error');
		}
   }
}

?>