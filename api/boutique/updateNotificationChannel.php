<?php

ini_set("display_errors", true);

require("../../config.php");
require("../../php/func_nx.php");
require("../../php/inc.appvars.php");

session_start();

$id = isset($_POST['id']) ? $_POST['id'] : null;
$name = isset($_POST['name']) ? $_POST['name'] : null;
$hotsos = isset($_POST['hotsos']) ? $_POST['hotsos'] : null;

//setup DB
$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

if ($id != null) {
	$sql = "UPDATE boutique_notificationchannel SET hotsos=:hotsos, lastUpdate=now(), lastUpdateBy=:lastUpdateBy WHERE id = '".$id."' ";

	$st = $conn->prepare($sql);

	$st->bindValue(":hotsos", $hotsos, PDO::PARAM_STR);
	$st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
} else {
	$sql = "INSERT INTO boutique_notificationchannel (name,hotsos,lastUpdate,lastUpdateBy) VALUES (:name,:hotsos,now(),:lastUpdateBy)";

	$st = $conn->prepare($sql);

	$st->bindValue(":name", $name, PDO::PARAM_STR);
	$st->bindValue(":hotsos", $hotsos, PDO::PARAM_STR);
	$st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
}

$st->execute();


if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'update notification good');
} else {
    echo returnStatus(0, 'update notification fail');
}

$conn = null;

?>
