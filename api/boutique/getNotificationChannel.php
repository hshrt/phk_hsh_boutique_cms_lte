<?php
require( "../../config.php" );

ini_set( "display_errors", true );

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "SELECT *
            FROM boutique_notificationchannel
            ORDER BY boutique_notificationchannel.id ASC";

$st = $conn->prepare($sql);

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$conn = null;

if($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'get config good', $list);
}
else{
    echo returnStatus(0, 'get config fail');
}

?>
