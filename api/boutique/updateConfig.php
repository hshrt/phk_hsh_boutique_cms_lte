<?php

ini_set("display_errors", true);

require("../../config.php");
require("../../php/func_nx.php");
require("../../php/inc.appvars.php");

session_start();

$id = isset($_POST['id']) ? $_POST['id'] : null;
$value = isset($_POST['value']) ? $_POST['value'] : null;

//setup DB
$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "UPDATE boutique_config SET value=:value, lastUpdate=now(), lastUpdateBy=:lastUpdateBy WHERE id = '".$id."' ";

//echo $sql;

$st = $conn->prepare($sql);

$st->bindValue(":value", $value, PDO::PARAM_STR);
$st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);

$st->execute();


if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'update config good');
} else {
    echo returnStatus(0, 'update config fail');
}

$conn = null;

?>
