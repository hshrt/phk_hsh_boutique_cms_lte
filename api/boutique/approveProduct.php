<?php

ini_set("display_errors", true);

require("../../config.php");
require("../../php/func_nx.php");
require("../../php/inc.appvars.php");

session_start();

$productId = isset($_POST['productId']) ? $_POST['productId'] : null;
$categoryId = isset($_POST['genreId']) ? $_POST['genreId'] : null;
$price = isset($_POST['price']) ? $_POST['price'] : null;
$posterurl = isset($_POST['posterurl']) ? $_POST['posterurl'] : null;
$posterurl1 = isset($_POST['posterurl1']) ? $_POST['posterurl1'] : null;
$posterurl2 = isset($_POST['posterurl2']) ? $_POST['posterurl2'] : null;
$posterurl3 = isset($_POST['posterurl3']) ? $_POST['posterurl3'] : null;
$language = isset($_POST['language']) ? $_POST['language'] : null;
$subtitle = isset($_POST['subtitle']) ? $_POST['subtitle'] : null;
$remark = isset($_POST['remark']) ? $_POST['remark'] : null;
$rating = isset($_POST['rating']) ? $_POST['rating'] : null;
$divisionId = isset($_POST['divisionId']) ? $_POST['divisionId'] : null;

//setup DB
$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "UPDATE boutique SET isAuth = 1 WHERE id = :productId;";

//echo $sql;

$st = $conn->prepare($sql);

$st->bindValue(":productId", $productId, PDO::PARAM_STR);

$st->execute();

if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {

    echo returnStatus(1, 'approval product good');
} else {
    echo returnStatus(0, 'approval product fail');
}


$conn = null;


?>