<?php

ini_set("display_errors", true);
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$movieId = isset($_POST['movieId']) ? $_POST['movieId'] : '';
$roomId = isset($_POST['roomId']) ? $_POST['roomId'] : '210';
$inventoryId = isset($_POST['inventoryId']) ? $_POST['inventoryId'] : '';
$statusId = isset($_POST['statusId']) ? $_POST['statusId'] : '1';
$requestId = isset($_POST['requestId']) ? $_POST['requestId'] : '';
$deliveryTime = isset($_POST['deliveryTime']) ? $_POST['deliveryTime'] : '';
$quantity = isset($_POST['quantity']) ? $_POST['quantity'] : '';
$session = ($_SESSION == null) ? "bedside" : $_SESSION['email'];

$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");


if(strlen($requestId) > 0){
    $sql = "UPDATE boutique_order_history SET inventoryId=:inventoryId, statusId=:statusId, lastUpdate=now(), lastUpdateBy=:lastUpdateBy WHERE id='".$requestId."' ";
    $st = $conn->prepare($sql);
    $st->bindValue(":inventoryId", $inventoryId, PDO::PARAM_STR);
    $st->bindValue(":statusId", $statusId, PDO::PARAM_STR);
    $st->bindValue(":lastUpdateBy", $session, PDO::PARAM_STR);
    $st->execute();

} else {

    $sql = "INSERT INTO boutique_order_history (roomId, requestTime, productId, reqDeliveryTime, inventoryId, enable, statusId, lastUpdate, lastUpdateBy, quantity) 
        VALUES (:roomId, now(), :productId, :reqDeliveryTime, :inventoryId, 1, :statusId, now(), :lastUpdateBy, :quantity)";
    $st = $conn->prepare($sql);
    $st->bindValue(":roomId", $roomId, PDO::PARAM_STR);
    $st->bindValue(":productId", $movieId, PDO::PARAM_STR);
    $st->bindValue(":inventoryId", $inventoryId, PDO::PARAM_STR);
    $st->bindValue(":statusId", $statusId, PDO::PARAM_STR);
	$st->bindValue(":reqDeliveryTime", $deliveryTime, PDO::PARAM_STR);
    $st->bindValue(":lastUpdateBy", $session, PDO::PARAM_STR);
	$st->bindValue(":quantity", $quantity, PDO::PARAM_STR);
    $st->execute();
}

if ($st->rowCount() > 0) {

    if(strlen($inventoryId) > 0) {
        $sql = "UPDATE boutique_inventory SET available=0, lastUpdate=now(), lastUpdateBy=:lastUpdateBy WHERE id ='" . $inventoryId . "'";
        $st = $conn->prepare($sql);
        $st->bindValue(":lastUpdateBy", $session, PDO::PARAM_STR);
        $st->execute();

        if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
            echo returnStatus(1, 'add borrow record good');
        } else {
            echo returnStatus(0, 'add borrow record fail');
        }
    } else {
        echo returnStatus(1, 'add borrow record good');
    }
} else {
    echo returnStatus(0, 'Add borrow record fail');
}


return 0;

?>
