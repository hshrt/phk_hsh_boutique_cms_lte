<?php

ini_set("display_errors", true);
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$stockId = isset($_POST['assetId']) ? $_POST['assetId'] : '';
$productId = isset($_POST['movieId']) ? $_POST['movieId'] : '';

$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "SELECT count(*) as totalNum 
                FROM boutique_inventory 
                WHERE boutique_inventory.isVoid = 0 
                      AND boutique_inventory.productId = '" . $productId . "'";
$st1 = $conn->prepare($sql);
$st1->execute();

$tempVar = 1;
while ($row2 = $st1->fetch(PDO::FETCH_ASSOC)) {
	$tempVar = $row2["totalNum"]+1;
}

$qtyToAdd = $stockId+$tempVar-1;
while($tempVar <= $qtyToAdd) {
//*****create Dictionary for Subject
$sql = "SELECT UUID() AS UUID";
$st2 = $conn->prepare($sql);
$st2->execute();

$list = array();

while ($row = $st2->fetch(PDO::FETCH_ASSOC)) {
    $list[] = $row;
}
$uuid = $list[0]["UUID"];


$sql = "INSERT INTO boutique_inventory (id, stockId, productId, available, isVoid, lastUpdate, lastUpdateBy)
          VALUES (:id, :stockId, :productId, :available, :isVoid, now(), :lastUpdateBy)";
$st = $conn->prepare($sql);
$st->bindValue(":id", $uuid, PDO::PARAM_STR);
$st->bindValue(":stockId", $tempVar, PDO::PARAM_INT);
$st->bindValue(":productId", $productId, PDO::PARAM_STR);
$st->bindValue(":available", 1, PDO::PARAM_INT);
$st->bindValue(":isVoid", 0, PDO::PARAM_STR);
$st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
$tempVar++;
$st->execute();
}

if ($st1->rowCount() > 0) {
    echo returnStatus(1, 'Add inventory OK ');
} else {
    echo returnStatus(0, 'Add inventory fail');
}

return 0;

?>
