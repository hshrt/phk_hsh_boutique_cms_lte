<?php
require( "../../config.php" );

ini_set( "display_errors", true );

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$page = 0;
$itemPerPage = 15;
$getCount = null;

if(isset($_REQUEST['getCount'])){
    $getCount = $_REQUEST['getCount'];
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

if(!empty($getCount)){
    $sql = "select  count(*) as totalNum from boutique_config ";
} else {
    $sql = "SELECT boutique_config.id,
                boutique_config.key,
                boutique_config.value,
                boutique_config.isNumeric
            FROM boutique_config 
            ORDER BY boutique_config.key ASC";
}

$st = $conn->prepare($sql);

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$conn = null;

if($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'get config good', $list);
}
else{
    echo returnStatus(0, 'get config fail');
}

?>
