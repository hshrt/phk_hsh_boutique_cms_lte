<?php
require("../../config.php");

ini_set("display_errors", true);

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$movieId = null;
$lang = null;

if (isset($_REQUEST['movieId'])) {
    $movieId = $_REQUEST['movieId'];
}

if (isset($_REQUEST['lang'])) {
    $lang = $_REQUEST['lang'];
} else {
    $lang = 'en';
}

$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "SELECT DISTINCT movies.id As id, title.movieTitle AS movieTitle, movies.genreId AS genreId, 
                        movies.year AS year, (CASE WHEN m1.available IS NULL THEN 0 ELSE m1.available END) AS available, movies.language As lang, 
                        movies.subtitle AS subtitle, movies.posterurl As poster, (CASE WHEN m2.stock IS NULL THEN 0 ELSE m2.stock END) As stock
        FROM movies
        LEFT JOIN (SELECT COUNT(movie_inventory.id) AS available,
                    movie_inventory.movieId As movieId
                  FROM movie_inventory 
                  WHERE available = 1 AND isVoid = 0 
                  GROUP BY  movie_inventory.movieId) m1
        ON movies.id = m1.movieId
        
         LEFT JOIN (SELECT COUNT(movie_inventory.id) AS stock,
                    movie_inventory.movieId As movieId
                  FROM movie_inventory 
                  WHERE isVoid = 0 
                  GROUP BY  movie_inventory.movieId) m2
        ON movies.id = m2.movieId
        
        INNER JOIN 
            (SELECT movies.titleId AS titleId, 
            (CASE movie_dictionary." . $lang . " WHEN '' THEN movie_dictionary.en ELSE movie_dictionary." . $lang . " END ) AS movieTitle
             FROM movies 
             INNER JOIN movie_dictionary
             ON movies.titleId = movie_dictionary.id) title
        ON title.titleId = movies.titleId ";

if (isset($_REQUEST['movieId'])) {
    $sql = $sql." AND movies.id = '".$movieId."' ";
}
$sql = $sql."GROUP BY title.movieTitle ORDER BY title.movieTitle ASC;";


$st = $conn->prepare($sql);

$st->execute();

$list = array();

while ($row = $st->fetch(PDO::FETCH_ASSOC)) {

    $genreId = $row["genreId"];
    $languageId = $row["lang"];
    $subtitleId = $row["subtitle"];

    $sql = "SELECT  movie_genre.id As id,
                movie_genre.titleId As titleId,
                movie_dictionary.".$lang." AS title,
                
            FROM movie_genre 
            INNER JOIN movie_dictionary
            ON movie_genre.titleId = movie_dictionary.id
            WHERE movie_genre.id IN (\"".$genreId."\")
            ORDER BY movie_genre.id ASC";

    $st_genre = $conn->prepare($sql);
    $st_genre->execute();
    $row_genreString = null;

    while ($row_genre = $st_genre->fetch(PDO::FETCH_ASSOC)) {

        if($row_genreString == null){
            $row_genreString = $row_genre["title"];
        } else {
            $row_genreString = $row_genreString.", ".$row_genre["title"];
        }
    }
    $row["genreId"] = 'XXXX';


    $list[] = $row;
}

$numberOfMovie = count($list);






$conn = null;

if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'get movieList good', $list);
} else {
    echo returnStatus(0, 'get movieList fail');
}

?>
