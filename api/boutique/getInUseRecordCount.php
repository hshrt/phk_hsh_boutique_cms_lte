<?php
require("../../config.php");

ini_set("display_errors", true);

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

$movieId = null;
$room = null;
$lang = "en";
$sqlForFilter = '';

if (isset($_REQUEST["room"]) && $_REQUEST["room"] != null && strlen($_REQUEST["room"]) > 1) {
    $room = $_REQUEST["room"];
    $sqlForFilter = " AND hist.roomId = '" . $room . "' AND (hist.statusId = 1 OR hist.statusId = 2 OR hist.statusId = 3) " ;
}

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

if (isset($_REQUEST["room"]) && $_REQUEST["room"] != null && strlen($_REQUEST["room"]) > 1) {

    $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
    $conn->exec("set names utf8");


    $sql = "SELECT count(*) as totalNum
            FROM boutique_order_history hist
            WHERE enable = 1 " . $sqlForFilter . "
            ORDER BY hist.requestTime DESC;";

    $st = $conn->prepare($sql);

    $st->execute();

    $list = array();

    while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
        $list[] = $row;
    }

    $conn = null;

    if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
        echo returnStatus(1, 'get in use record good', $list);
    } else {
        echo returnStatus(0, 'get in use record fail');
    }
}
?>
