<?php

ini_set("display_errors", true);

require("../../config.php");
require("../../php/func_nx.php");
require("../../php/inc.appvars.php");

session_start();

$movieTitleId = isset($_POST['movieTitleId']) ? $_POST['movieTitleId'] : null;
$descriptionId = isset($_POST['descriptionId']) ? $_POST['descriptionId'] : null;
$genreId = isset($_POST['genreId']) ? $_POST['genreId'] : null;
$year = isset($_POST['year']) ? $_POST['year'] : null;
$posterurl = isset($_POST['posterurl']) ? $_POST['posterurl'] : null;
$language = isset($_POST['language']) ? $_POST['language'] : null;
$subtitle = isset($_POST['subtitle']) ? $_POST['subtitle'] : null;
$stockChanged = isset($_POST['stockChanged']) ? $_POST['stockChanged'] : null;

//setup DB
$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");


if($stockChanged < 0){

    $stockChanged = $stockChanged * -1;

    $sql = "DELETE FROM boutique WHERE titleId = '"
        .$movieTitleId."' AND enable = 1 AND available = 1 LIMIT ".$stockChanged;

    $st = $conn->prepare($sql);
    $st->execute();
    if ($st->rowCount() > 0) {
        echo returnStatus(1, 'update boutique stock good ');
    } else {
        echo returnStatus(0, 'update boutique stock fail');
    }

} else if ($stockChanged > 0){
    for ($x = 0; $x < $stockChanged; $x++) {
        $sql = "SELECT UUID() AS UUID";
        $st = $conn->prepare($sql);
        $st->execute();

        $list = array();

        while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
            $list[] = $row;
        }
        $uuid = $list[0]["UUID"];

        $sql = "INSERT INTO boutique (id, titleId, descriptionId, categoryId, price, posterurl, enable, lastUpdate, lastUpdateBy) 
                VALUES (:id, :titleId, :descriptionId, :genreId, :year, :posterurl, :enable, now(),:lastUpdateBy)";
        $st = $conn->prepare($sql);
        $st->bindValue(":id", $uuid, PDO::PARAM_STR);
        $st->bindValue(":titleId", $movieTitleId, PDO::PARAM_STR);
        $st->bindValue(":descriptionId", $descriptionId, PDO::PARAM_STR);
        $st->bindValue(":genreId", $genreId, PDO::PARAM_STR);
        $st->bindValue(":year", $year, PDO::PARAM_STR);
        $st->bindValue(":posterurl", $posterurl, PDO::PARAM_STR);
        $st->bindValue(":enable", 1, PDO::PARAM_INT);
        $st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
        $st->execute();

        if ($st->rowCount() > 0) {
            echo returnStatus(1, 'update boutique stock good ');
        } else {
            echo returnStatus(0, 'update boutique stock fail');
        }
    }
}
?>
