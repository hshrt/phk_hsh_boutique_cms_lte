<?php

ini_set("display_errors", true);

require("../../config.php");
require("../../php/func_nx.php");
require("../../php/inc.appvars.php");

session_start();

$productId = isset($_POST['productId']) ? $_POST['productId'] : null;
$categoryId = isset($_POST['genreId']) ? $_POST['genreId'] : null;
$price = isset($_POST['price']) ? $_POST['price'] : null;
$posterurl = isset($_POST['posterurl']) ? $_POST['posterurl'] : null;
$posterurl1 = isset($_POST['posterurl1']) ? $_POST['posterurl1'] : null;
$posterurl2 = isset($_POST['posterurl2']) ? $_POST['posterurl2'] : null;
$posterurl3 = isset($_POST['posterurl3']) ? $_POST['posterurl3'] : null;
$language = isset($_POST['language']) ? $_POST['language'] : null;
$subtitle = isset($_POST['subtitle']) ? $_POST['subtitle'] : null;
$remark = isset($_POST['remark']) ? $_POST['remark'] : null;
$rating = isset($_POST['rating']) ? $_POST['rating'] : null;
$divisionId = isset($_POST['divisionId']) ? $_POST['divisionId'] : null;
$hotsos = isset($_POST['hotsos']) ? $_POST['hotsos'] : null;

//setup DB
$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "UPDATE boutique SET isAuth = 0, price=:price, posterurl=:posterurl, posterurl1=:posterurl1, posterurl2=:posterurl2, posterurl3=:posterurl3, rating=:rating, remark=:remark, divisionId=:divisionId, notificationChannels=:hotsos, lastUpdate=now(),lastUpdateBy=:lastUpdateBy WHERE id = :productId;";

//echo $sql;

$st = $conn->prepare($sql);

$st->bindValue(":productId", $productId, PDO::PARAM_STR);
$st->bindValue(":price", $price, PDO::PARAM_STR);
$st->bindValue(":posterurl", $posterurl, PDO::PARAM_STR);
$st->bindValue(":posterurl1", $posterurl1, PDO::PARAM_STR);
$st->bindValue(":posterurl2", $posterurl2, PDO::PARAM_STR);
$st->bindValue(":posterurl3", $posterurl3, PDO::PARAM_STR);
$st->bindValue(":rating", $rating, PDO::PARAM_INT);
$st->bindValue(":remark", $remark, PDO::PARAM_INT);
$st->bindValue(":divisionId", $divisionId, PDO::PARAM_STR);
$st->bindValue(":hotsos", $hotsos, PDO::PARAM_STR);
$st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);

$st->execute();

if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {


    $sql = "DELETE FROM boutiques_category WHERE productId = '" .$productId."'" ;
    $st = $conn->prepare($sql);
    $st->execute();

    $genreArray = explode(',', $categoryId);
    foreach ($genreArray as &$value){
        $sql = "INSERT INTO 
            boutiques_category (productId, categoryId, lastUpdate, lastUpdateBy) 
            VALUES (:productId, :categoryId,  now(),:lastUpdateBy)";
        $st = $conn->prepare($sql);
        $st->bindValue(":productId", $productId, PDO::PARAM_STR);
        $st->bindValue(":categoryId", $value, PDO::PARAM_STR);
        $st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
        $st->execute();
    }
    echo returnStatus(1, 'update product good');
} else {
    echo returnStatus(0, 'update product fail');
}


$conn = null;


?>
