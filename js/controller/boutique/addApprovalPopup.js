App.AddApprovalPopup = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    self: null,
    room: "",
    keyObj: "",
    keyObjects: null,
    movieId: "",

    // It's the first function called when this view it's instantiated.
    initialize: function (options) {
        this.self = this;
        if (options && options.keyObj) {
            this.keyObj = options.keyObj;
            movieId = options.keyObj.movieId;

        }
        this.render();
    },
    events: {
        'click #closeBtn': 'destroy'
    },
    setupUI: function () {
        var _self = this;
    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function () {

        var _self = this;
		var index = $(this).attr("order");

                App.yesNoPopup = new App.YesNoPopup(
                    {
                        yesFunc: function () {

                            $.ajax({
                                url: "api/boutique/approveProduct.php",
                                method: "POST",
                                dataType: "json",
                                data: {
                                    productId: movieId
                                }
                            }).success(function (json) {
                                console.log(json);

                                if (json.status == 502) {
                                    alert(App.strings.sessionTimeOut);
                                    $('#addNewInventoryBtn').off('click')
                                    $('#delete').off('click')
                                    _self.setupUI();
                                    return;
                                }
                                App.yesNoPopup.destroy();
                                $('#addNewInventoryBtn').off('click')
                                $('#delete').off('click')
								App.pendingList.refresh();
                                _self.setupUI();

                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });
                        },
						cancelFunc: function () {
							App.pendingList.isButtonClicked = false;
						},
                        msg: "Are you sure to approve this item?"
                    }
                );
    },
    showUp: function () {
        $(this.el).show();
        this.isHide = false;
    },
    close: function () {
        console.log("close fire");
    },
    destroy: function () {
        $(".popup_box_container").remove();

        this.undelegateEvents();
    },
    isHide: false
});		