App.DeleteList = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    keyObjects:null,
    keyObjectsFiltered:null,
    page:0,
    itemPerPage:15,
    filtering:false,
    initialize: function(options){
        if(options && options.listTitle){
            this.title = options.listTitle;
        }
        this.render();

    },
    events: {
    },

    render: function(){

        //alert($(window).width());
        var self = this;
        $.ajax({
            url : "php/html/listView.php?type=delete",
            method : "GET",
            dataType: "html",
        }).success(function(html){
            self.updateNavigation(1);
            self.loadItem(self.page);
            $(self.el).append(html).
                promise()
                .done(function(){
                });

        }).error(function(d){
            console.log('error');
            console.log(d);
        });

    },
    updateNavigation: function(_items){

        var self = this;

        $("#paginationContainer").pagination({
            items: _items,
            itemsOnPage: self.itemPerPage,
            cssStyle: 'light-theme',
            onPageClick:function(pageNum, event){
                self.page = pageNum -1;
                self.loadItem(pageNum-1);
            }
        });
    },
    goBackToFirstPage: function(){
        $("#paginationContainer").pagination('selectPage', 1);
    },
    refresh: function(){
        var self = this;
        self.keyObjects = null;
        self.keyObjectsFiltered = null;
        self.loadItem(0);
    },
    loadItem: function(page){

        var self = this;

        $("#itemContainer").empty();

            $.ajax({
                url : "api/boutique/getConfigList.php",
                method : "GET",
                cache: false,
                dataType: "json"
            }).success(function(json) {

				var x=0;
				var itemRoot = document.createElement('div');
				$(itemRoot).addClass("itemRoot");
	
				$("#itemContainer").append($(itemRoot));

				$(itemRoot).append('<div id=' + 'key' + x + '></div>');
				$("#key" + x).text("Delete old data (Permanently deletes history of records and images for the products which are already deleted from 'Boutique Inventory' tab)");
				$("#key" + x).addClass("listText_sc");

				$(itemRoot).append("<div id=submit" + x + " class=\"borrow\" order=" + x + " title='click to delete'" + "> " +
					"<button type=\"button\" class=\"btn btn-default \" >" +
						"Delete" +
							"</button>" +
								"</div>");

				$("#submit"+x).on('click',function(){
					var index = $(this).attr("order");
					var value = $("#value"+index).val();
	
					$.ajax({
						url: "api/boutique/deleteOldData.php",
						method: "POST",
						dataType: "json",
						data: {}
					}).success(function (json) {
						console.log(json);
						if (json.status == 502) {
							alert(App.strings.sessionTimeOut);
							//_self.setupUI();
							return;
						}
						if (json.status == 1) {
							alert("Deletion Succeed");
						} else if (json.status == 0) {
							alert("Nothing to delete");
						}
						//_self.setupUI();
					}).error(function (d) {
						console.log('error');
						console.log(d);
					});
				});
			}).error(function(d){
                console.log('error');
                console.log(d);
        });
   },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW


        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});