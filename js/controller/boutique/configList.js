App.ConfigList = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    keyObjects:null,
    keyObjectsFiltered:null,
    page:0,
    itemPerPage:15,
    filtering:false,
    initialize: function(options){
        if(options && options.listTitle){
            this.title = options.listTitle;
        }
        this.render();

    },
    events: {
    },

    render: function(){

        //alert($(window).width());
        var self = this;
        $.ajax({
            url : "php/html/listView.php?type=config",
            method : "GET",
            dataType: "html",
        }).success(function(html){

            $(self.el).append(html).
                promise()
                .done(function(){

                });

            $.ajax({
                url : "api/boutique/getConfigList.php",
                method : "GET",
                dataType: "json",
                cache: false,
                data : {getCount: true}
            }).success(function(json){

                self.updateNavigation(json.data[0].totalNum);
                self.loadItem(self.page);

            }).error(function(d){
                console.log('error');
                console.log(d);
            });

        }).error(function(d){
            console.log('error');
            console.log(d);
        });

    },
    updateNavigation: function(_items){

        var self = this;

        $("#paginationContainer").pagination({
            items: _items,
            itemsOnPage: self.itemPerPage,
            cssStyle: 'light-theme',
            onPageClick:function(pageNum, event){
                self.page = pageNum -1;
                self.loadItem(pageNum-1);
            }
        });
    },
    goBackToFirstPage: function(){
        $("#paginationContainer").pagination('selectPage', 1);
    },
    refresh: function(){
        var self = this;
        self.keyObjects = null;
        self.keyObjectsFiltered = null;
        self.loadItem(0);
    },
    loadItem: function(page){

        var self = this;

        $("#itemContainer").empty();

        if(self.keyObjects == null){
            $.ajax({
                url : "api/boutique/getConfigList.php",
                method : "GET",
                cache: false,
                dataType: "json"
            }).success(function(json){

                self.keyObjects =  json.data;

                   //alert(self.keyObjects.length);
                //
                // $("#paginationContainer").pagination('updateItems', self.keyObjects.length);


                for(var x = page*self.itemPerPage; x < self.itemPerPage*(page+1) && x < self.keyObjects.length; x++) {

                    var itemRoot = document.createElement('div');
                    $(itemRoot).addClass("itemRoot");

                    $("#itemContainer").append($(itemRoot));

                    $(itemRoot).append('<div id=' + 'key' + x + '></div>');
                    $("#key" + x).text(self.keyObjects[x].key);
                    $("#key" + x).addClass("listText_sc");

                    $(itemRoot).append("<div id=submit" + x + " class=\"borrow\" order=" + x + " title='click to submit'" + "> " +
                        "<button type=\"button\" class=\"btn btn-default \" >" +
                        "Submit" +
                        "</button>" +
                        "</div>");

                    if (self.keyObjects[x].isNumeric == 0) {
                        $(itemRoot).append("<input type='text' id=value" + x + "></input>");
                        $("#value"+x).val(self.keyObjects[x].value);
                    } else {
                        $(itemRoot).append("<input type='number' id=value" + x + "></input>");
                        $("#value"+x).val(parseInt(self.keyObjects[x].value));
                    }

                    $("#submit"+x).on('click',function(){
                        var index = $(this).attr("order");
                        var value = $("#value"+index).val();

                        $.ajax({
                            url: "api/boutique/updateConfig.php",
                            method: "POST",
                            dataType: "json",
                            data: {
                                id: self.keyObjects[index].id,
                                value:value
                            }
                        }).success(function (json) {
                            console.log(json);
                            if (json.status == 502) {
                                alert(App.strings.sessionTimeOut);
                                _self.setupUI();
                                return;
                            }
                            alert("Update Succeed");
                            _self.setupUI();
                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });
                    });
                }


            }).error(function(d){
                console.log('error');
                console.log(d);
            });
        }
        else{

            var keyObj = self.filtering?self.keyObjectsFiltered:self.keyObjects;

            var upperLimit = keyObj.length<self.itemPerPage?keyObj.length:self.itemPerPage;

            $("#paginationContainer").pagination('updateItems', keyObj.length);

            for(var x = page*self.itemPerPage; x < self.itemPerPage*(page+1) && x < self.keyObjects.length; x++) {

                var itemRoot = document.createElement('div');
                $(itemRoot).addClass("itemRoot");

                $("#itemContainer").append($(itemRoot));

                $(itemRoot).append('<div id=' + 'key' + x + '></div>');
                $("#key" + x).text(self.keyObj[x].key);
                $("#key" + x).addClass("listText_sc");

                $(itemRoot).append("<div id=submit" + x + " class=\"borrow\" order=" + x + " title='click to submit'" + "> " +
                    "<button type=\"button\" class=\"btn btn-default \" >" +
                    "Submit" +
                    "</button>" +
                    "</div>");

                if (self.keyObjects[x].isNumeric == 0) {
                    $(itemRoot).append("<input type='text' id=value" + x + "></input>");
                    $("#value"+x).val(self.keyObj[x].value);
                } else {
                    $(itemRoot).append("<input type='number' id=value" + x + "></input>");
                    $("#value"+x).val(parseInt(self.keyObj[x].value));
                }

                $("#submit"+x).on('click',function(){
                    var index = $(this).attr("order");
                    var value = $("#value"+index).val();

                    $.ajax({
                        url: "api/boutique/updateConfig.php",
                        method: "POST",
                        dataType: "json",
                        data: {
                            id: self.keyObj[index].id,
                            value:value
                        }
                    }).success(function (json) {
                        console.log(json);
                        if (json.status == 502) {
                            alert(App.strings.sessionTimeOut);
                            _self.setupUI();
                            return;
                        }
                        alert("Update Succeed");
                        _self.setupUI();
                    }).error(function (d) {
                        console.log('error');
                        console.log(d);
                    });
                });
            }
        }
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW


        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});