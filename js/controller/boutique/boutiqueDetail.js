App.BoutiqueDetail = Backbone.View.extend({

    el: '#content_container',
    uploadImageSizeLimit: 2,
    posterWidth: 270,
    posterHeight: 400,

    currentLang: "en",
    jsonObj: null,

    type: null,
    movieId: null,
    titleLangModel: {},
    desLangModel: {},

    genreList: [],
    selected_genre: [],

    hotsosList: [],
    selected_hotsos: "",

    languageList: [],
    selected_language: [],

    subtitleList: [],
    selected_subtitle: [],
	posterIndex: [],

    divisionList: [],
    selected_division: "",

    rating: 0,

    posterFilePath: null,
	posterFilePath1: [],
    price: null,
	remark: "",

    isNewPoster: false,
	
	index: 0,

    title: "Movie",

    initialize: function (options) {
        this.selectGuestModel = [];
        if (options && options.id) {
            this.movieId = options.id;
        }
        if (options && options.type) {
            this.type = options.type;
        }
        this.render();
    },
    events: {
        'click #closeBtn_start': 'backToIndex'
    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function () {

        var self = this;
        //remove any cached data if it is for new message
        if (self.type == "new") {
			App.imageData = null;
			App.imageData1 = null;
			App.imageData2 = null;
			App.imageData3 = null;
            App.currentItem = [];
            App.currentId = "";
            self.selected_genre = [];
            self.selected_hotsos = "";
			self.posterFilePath1 = [];
			self.posterIndex = [0,0,0,0];
            self.selected_language = [];
            self.selected_subtitle = [];
            self.titleLangModel = [];
            self.desLangModel = [];
            self.selected_division = "";
            self.rating = 0;
			self.remark = "";
        } else {
            App.currentItem = [];
            App.currentId = this.movieId;
			self.posterIndex = [0,0,0,0];
			/*App.imageData = null;
			App.imageData1 = null;
			App.imageData2 = null;
			App.imageData3 = null;*/
        }

        $.ajax({
            url: "php/html/boutiqueDetail.php",
            method: "GET",
            dataType: "html",
            data: {}
        }).success(function (html) {
            $(self.el).append(html).promise()
                .done(function () {
					$("#imageContainer1").hide();
					$("#imageContainer2").hide();
					$("#imageContainer3").hide();
					$("#ratingLabel").hide();
					$("#ratingInput").hide();

                    CKEDITOR.replace('editor1');

                    CKEDITOR.replace('editor2', {
                        on: {
                            focus: function () {
                                console.log("focus editor2")
                            },
                            blur: function () {
                                self.saveDescriptionLang()
                            }
                        }
                    });

                    //this is editing existed message
                    if (self.type != "new") {

                        $.ajax({
                            url: "api/boutique/getMovie.php",
                            method: "GET",
                            cache: false,
                            dataType: "json",
                            data: {movieId: App.currentId}
                        }).success(function (json) {
                            App.currentItem = json.data[0];
                            self.getSelectedGerne(self);
                            self.getSelectedHotsos(self);
                            self.getSelectedLanguage(self);
                            self.getSelectedSubtitle(self);
                            self.getSelectedDivision(self);
                            self.getRating(self);
							self.getRemark(self);
                            self.posterFilePath1[0] = App.currentItem.poster;
							self.posterFilePath1[1] = App.currentItem.poster1;
							self.posterFilePath1[2] = App.currentItem.poster2;
							self.posterFilePath1[3] = App.currentItem.poster3;
                            self.price = App.currentItem.price;
                            self.setupUIHandler();

                            //get language info for item Title
                            $.ajax({
                                url: "api/boutique/getLanguageMap.php",
                                method: "POST",
                                dataType: "json",
                                data: {id: App.currentItem.titleId}
                            }).success(function (json) {
                                console.log(json.data[0]);
                                self.titleLangModel = json.data[0];
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

                            //get language info for item Description
                            $.ajax({
                                url: "api/boutique/getLanguageMap.php",
                                method: "POST",
                                dataType: "json",
                                data: {id: App.currentItem.descriptionId}
                            }).success(function (json) {
                                console.log(json.data[0]);
                                self.desLangModel = json.data[0];
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

                            //get genre list
                            $.ajax({
                                url: "api/boutique/getCategoryList.php",
                                method: "GET",
                                dataType: "json",
                                data: {}
                            }).success(function (json) {
                                self.genreList = json.data;
                                self.initGenreList(self);
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

                            $.ajax({
                                url: "api/boutique/getNotificationChannel.php",
                                method: "GET",
                                dataType: "json",
                                data: {}
                            }).success(function (json) {
                                self.hotsosList = json.data;
                                self.initHotsosList(self);
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

                            //get language list
                            $.ajax({
                                url: "api/boutique/getLanguageList.php",
                                method: "GET",
                                dataType: "json",
                                data: {}
                            }).success(function (json) {
                                self.languageList = json.data;
                                self.initLanguageList(self);
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

                            //get subtitle list
                            $.ajax({
                                url: "api/boutique/getSubtitleList.php",
                                method: "GET",
                                dataType: "json",
                                data: {}
                            }).success(function (json) {
                                self.subtitleList = json.data;
                                self.initSubtitleList(self);
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

                            //get division list
                            $.ajax({
                                url: "api/boutique/getDivisionList.php",
                                method: "GET",
                                dataType: "json",
                                data: {}
                            }).success(function (json) {
                                self.divisionList = json.data;
                                self.initDivisionList(self);
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

							
							self.initRemark(self);
                            self.initRating(self);
							self.initPrice(self);

                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });
                    }
                    else {//this is for new message
                        self.setupUIHandler();

                        //get genre list
                        $.ajax({
                            url: "api/boutique/getCategoryList.php",
                            method: "GET",
                            dataType: "json",
                            data: {}
                        }).success(function (json) {
                            self.genreList = json.data;
                            self.initGenreList(self);
                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });

                        $.ajax({
                            url: "api/boutique/getNotificationChannel.php",
                            method: "GET",
                            dataType: "json",
                            data: {}
                        }).success(function (json) {
                            self.hotsosList = json.data;
                            self.initHotsosList(self);
                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });

                        //get language list
                        $.ajax({
                            url: "api/boutique/getLanguageList.php",
                            method: "GET",
                            dataType: "json",
                            data: {}
                        }).success(function (json) {
                            self.languageList = json.data;
                            self.initLanguageList(self);
                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });

                        //get subtitle list
                        $.ajax({
                            url: "api/boutique/getSubtitleList.php",
                            method: "GET",
                            dataType: "json",
                            data: {}
                        }).success(function (json) {
                            self.subtitleList = json.data;
                            self.initSubtitleList(self);
                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });

                        //get division list
                        $.ajax({
                            url: "api/boutique/getDivisionList.php",
                            method: "GET",
                            dataType: "json",
                            data: {}
                        }).success(function (json) {
                            self.divisionList = json.data;
                            self.initDivisionList(self);
                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });

						self.initRemark(self);
                        self.initRating(self);
						self.initPrice(self);
                    }

                    setTimeout(function () {
                        var editor2 = CKEDITOR.instances.editor2;
                        editor2.setReadOnly(true);
                    }, 1000);

                });

        }).error(function (d) {
            console.log('error');
            console.log(d);
        });


    },
    showPopup: function () {
        var self = this;
        //alert(self.title);
        App.createItemPopup = new App.CreateItemPopup(
            {

                title: self.title == "Library" ? "Page" : self.title,
                parentId: self.parentId
            }
        );
    },
    backToIndex: function () {
        window.parent.PeriEvent.backToIndex();
    },
    setupUIHandler: function () {
        var self = this;

        //pre-fill the form
        self.type != "new" ? $('#itemName').text("Edit Product") : $('#itemName').text("New Product");

        if (self.type != "new") {
            self.loadPhoto();
        //    self.loadPrice();
		//	self.loadRemark();
        }

        $('#container_en input').val(App.currentItem.movieTitle);
        $('#container_lang input').val(App.currentItem.movieTitle);

        var editor_en = CKEDITOR.instances.editor1;
        var editor_lang = CKEDITOR.instances.editor2;

        setTimeout(function () {
            var string = "";

            if (self.desLangModel.en.indexOf("<br />") == -1 && App.currentItem.lastUpdateBy == "TC3") {
                string = self.desLangModel.en.replace(/\n/g, "<br />");
            }
            else {
                string = self.desLangModel.en;
            }
            editor_en.setData(string);
        }, 1000);

		if (App.userEmail == "rolfbuehlmann@peninsula.com" && !String(window.location).includes("pending")) {
			$("#save_btn").hide();
		}

        if (self.type == "new") {
            $("#delete_btn").hide();
        } else {
            //get count
            $.ajax({
                url: "api/boutique/getCount.php",
                method: "GET",
                dataType: "json",
                data: {type: "movie",
                        id: self.movieId
                }
            }).success(function (json) {
                if(json.data[0].totalNum > 0 || App.userEmail == "rolfbuehlmann@peninsula.com"){
                    $("#delete_btn").hide();
                }
            }).error(function (d) {
                console.log('error');
                console.log(d);
            });

        }

        //handle delete button
        $('#delete_btn').on('click', function () {
            App.yesNoPopup = new App.YesNoPopup(
                {
                    yesFunc: function () {
                        $.ajax({
                            url: "api/boutique/deleteProduct.php",
                            method: "POST",
                            dataType: "json",
                            data: {id: App.currentId}
                        }).success(function (json) {
                            if (json.status == 502) {
                                alert(App.strings.sessionTimeOut);
                                location.reload();
                                return;
                            }
                            App.yesNoPopup.destroy();
                            App.goUpper2Level();

                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });
                    },
                    msg: "Are you sure to delete this product?"
                }
            );
        });

        //handle preview button
        $('#preview_btn').on('click', function () {
            self.saveDescriptionLang();

            console.log(editor_en.getData());

            var currentLang = $("#lang_selectionBox").val(),
                toSendTitle = $('#detailTitle_en').val(),
                toSendDes = editor_en.getData();                

            if (currentLang != "en"){
                toSendTitle = eval("self.titleLangModel."+$("#lang_selectionBox").val());
                toSendDes = eval("self.desLangModel."+$("#lang_selectionBox").val());
                
            }

            var genre = "";
            var price = $("#priceInput").val();
            
            
            
            for (var x = 0; x < self.genreList.length; x++) {
              var isChecked = false;
              for (var y = 0; y < self.selected_genre[y]; y++) {
                  if (self.genreList[x].id == self.selected_genre[y]) {
                      isChecked = true;
                      break;
                  }
              }
  
              if (isChecked) {
                if ( genre == "" ) 
                  genre = genre + "" + self.genreList[x].en + "";
                else
                  genre = genre + "," + self.genreList[x].en + "";
              } 
            }
            
            console.log(self.selected_genre.join(','));

            App.previewPopup = new App.PreviewPopup(
                {
                    root:self,
                    previewType: "boutique",
                    itemTitle: toSendTitle,
                    description: toSendDes,
                    lang: currentLang,
                    price: price,
                    img1: $('#uploadImage').attr('src'),
                    img2: $('#uploadImage1').attr('src'),
                    img3: $('#uploadImage2').attr('src'),
                    img4: $('#uploadImage3').attr('src'),                
                    catText: genre
                }
            );

        });

        //handle save button
        $('#save_btn').on('click', function () {

            if ($('#detailTitle_en').val() != null && $('#detailTitle_en').val().length < 1) {
                alert("Please input Product Title .");
                return;
            }

            if ( editor_en.getData() != null &&  editor_en.getData().length < 1) {
                alert("Please input Product Description .");
                return;
            }

            if (($("#priceInput").val()) == null || ($("#priceInput").val()).length < 1) {
                alert("Please fill in Price.");
                return;
            }

            if (self.selected_genre[0] == null) {
                alert("Please select at least one category.");
                return;
            }
            if (self.type == "new") {
                if (!App.imageData || !App.imageFileName) {
                    alert("Please upload first image");
                    return;
                }
            }

            App.yesNoPopup = new App.YesNoPopup(
                {
                    yesFunc: function () {
                        App.showLoading();

                        //save the description for other language first
                        self.saveDescriptionLang();

                        //updateItem exist message
                        if (self.type != "new") {
                            //updateItem Title of Item

                            var selectedLanguageList = '';
                            var selectedSubtitleList = '';
                            var selectedGenreList = '';
                            var selectedHotsosList = '';

                            $.ajax({
                                url: "api/boutique/updateDict.php",
                                method: "POST",
                                dataType: "text",
                                data: {
                                    title_id: App.currentItem.titleId,
                                    title_en: $('#detailTitle_en').val(),
                                    title_zh_hk: self.titleLangModel.zh_hk,
                                    title_zh_cn: self.titleLangModel.zh_cn,
                                    title_jp: self.titleLangModel.jp,
                                    title_fr: self.titleLangModel.fr,
                                    title_ar: self.titleLangModel.ar,
                                    title_es: self.titleLangModel.es,
                                    title_de: self.titleLangModel.de,
                                    title_ko: self.titleLangModel.ko,
                                    title_ru: self.titleLangModel.ru,
                                    title_pt: self.titleLangModel.pt
                                }
                            }).success(function (json) {

                                console.log(json);
                                if (json.status == 502) {
                                    alert(App.strings.sessionTimeOut);
                                    location.reload();
                                    return;
                                }

                                //updateItem Description of Item
                                $.ajax({
                                    url: "api/boutique/updateDict.php",
                                    method: "POST",
                                    dataType: "text",
                                    data: {
                                        title_id: App.currentItem.descriptionId,
                                        title_en: editor_en.getData(),
                                        title_zh_hk: self.desLangModel.zh_hk,
                                        title_zh_cn: self.desLangModel.zh_cn,
                                        title_jp: self.desLangModel.jp,
                                        title_fr: self.desLangModel.fr,
                                        title_ar: self.desLangModel.ar,
                                        title_es: self.desLangModel.es,
                                        title_de: self.desLangModel.de,
                                        title_ko: self.desLangModel.ko,
                                        title_ru: self.desLangModel.ru,
                                        title_pt: self.desLangModel.pt
                                    }
                                }).success(function (json) {



                                    if (App.boutiqueDetail.isNewPoster) {

									   if (App.boutiqueDetail.posterIndex[0] == 1)
										App.imageData = $('#imageContainer > img').cropper("getDataURL", { width:App.boutiqueDetail.posterWidth , height: App.boutiqueDetail.posterHeight }).replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
									else
										App.imageData = null;
									if (App.boutiqueDetail.posterIndex[1] == 1)
										App.imageData1 = $('#imageContainer1 > img').cropper("getDataURL", { width:App.boutiqueDetail.posterWidth , height: App.boutiqueDetail.posterHeight }).replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
									else
										App.imageData1 = null;
									if (App.boutiqueDetail.posterIndex[2] == 1)
										App.imageData2 = $('#imageContainer2 > img').cropper("getDataURL", { width:App.boutiqueDetail.posterWidth , height: App.boutiqueDetail.posterHeight }).replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
									else
										App.imageData2 = null;
									if (App.boutiqueDetail.posterIndex[3] == 1)
										App.imageData3 = $('#imageContainer3 > img').cropper("getDataURL", { width:App.boutiqueDetail.posterWidth , height: App.boutiqueDetail.posterHeight }).replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
									else
										App.imageData3 = null;
									
                                        $.ajax({
                                            url: "api/boutique/uploadPhoto.php",
                                            method: "POST",
                                            dataType: "json",
											data: {
												image: App.imageData,
												image1: App.imageData1,
												image2: App.imageData2,
												image3: App.imageData3
											}
                                        }).success(function (json) {
                                            console.log(json);

                                            if (json.status == 502) {
                                                alert(App.strings.sessionTimeOut);
                                                location.reload();
                                                return;
                                            }

											if (App.imageData != null)
											self.posterFilePath1[0] = json.data.filepath;
										if (App.imageData1 != null)
											self.posterFilePath1[1] = json.data.filepath1;
										if (App.imageData2 != null)
											self.posterFilePath1[2] = json.data.filepath2;
										if (App.imageData3 != null)
											self.posterFilePath1[3] = json.data.filepath3;

                                            for (var x = 0; x < self.selected_genre.length; x++) {
                                                if (x != self.selected_genre.length - 1) {
                                                    selectedGenreList += self.selected_genre[x] + ",";
                                                }
                                                else {
                                                    selectedGenreList += self.selected_genre[x];
                                                }
                                            }

                                            selectedHotsosList = self.selected_hotsos;

                                            for (var x = 0; x < self.selected_language.length; x++) {
                                                if (x != self.selected_language.length - 1) {
                                                    selectedLanguageList += self.selected_language[x] + ",";
                                                }
                                                else {
                                                    selectedLanguageList += self.selected_language[x];
                                                }
                                            }

                                            for (var x = 0; x < self.selected_subtitle.length; x++) {
                                                if (x != self.selected_subtitle.length - 1) {
                                                    selectedSubtitleList += self.selected_subtitle[x] + ",";
                                                }
                                                else {
                                                    selectedSubtitleList += self.selected_subtitle[x];
                                                }
                                            }

                                            $.ajax({
                                                url: "api/boutique/updateProduct.php",
                                                method: "POST",
                                                dataType: "json",
                                                data: {
                                                    productId: App.currentId,
                                                    genreId: selectedGenreList,
                                                    price: $("#priceInput").val(),
													remark: $("#remarkInput").val(),
                                                    posterurl: self.posterFilePath1[0],
													posterurl1: self.posterFilePath1[1],
													posterurl2: self.posterFilePath1[2],
													posterurl3: self.posterFilePath1[3],
                                                    language: selectedLanguageList,
                                                    subtitle: selectedSubtitleList,
                                                    rating: parseFloat($("#ratingInput").val()).toFixed(1),
                                                    divisionId: $('#divisionSelector').find(":selected").val(),
                                                    hotsos: selectedHotsosList,
                                                }
                                            }).success(function (json) {
                                                console.log(json);
                                                App.goUpper2Level();
                                            }).error(function (d) {
                                                console.log('error');
                                                console.log(d);
                                            });
                                            // }


                                        }).error(function (d) {
                                            console.log('error');
                                            console.log(d);
                                            alert("Upload photo error, please try again.");
                                            self.destroy();
                                        });
                                    } else {
                                        //updateItem Description of Item

                                        for (var x = 0; x < self.selected_genre.length; x++) {
                                            if (x != self.selected_genre.length - 1) {
                                                selectedGenreList += self.selected_genre[x] + ",";
                                            }
                                            else {
                                                selectedGenreList += self.selected_genre[x];
                                            }
                                        }

                                        selectedHotsosList = self.selected_hotsos;

                                        for (var x = 0; x < self.selected_language.length; x++) {
                                            if (x != self.selected_language.length - 1) {
                                                selectedLanguageList += self.selected_language[x] + ",";
                                            }
                                            else {
                                                selectedLanguageList += self.selected_language[x];
                                            }
                                        }

                                        for (var x = 0; x < self.selected_subtitle.length; x++) {
                                            if (x != self.selected_subtitle.length - 1) {
                                                selectedSubtitleList += self.selected_subtitle[x] + ",";
                                            }
                                            else {
                                                selectedSubtitleList += self.selected_subtitle[x];
                                            }
                                        }

                                        $.ajax({
                                            url: "api/boutique/updateProduct.php",
                                            method: "POST",
                                            dataType: "json",
                                            data: {
                                                productId: App.currentId,
                                                genreId: selectedGenreList,
                                                price: $("#priceInput").val(),
												remark: $("#remarkInput").val(),
                                                posterurl: self.posterFilePath1[0],
												posterurl1: self.posterFilePath1[1],
												posterurl2: self.posterFilePath1[2],
												posterurl3: self.posterFilePath1[3],
                                                language: selectedLanguageList,
                                                subtitle: selectedSubtitleList,
                                                rating: $("#ratingInput").val(),
                                                divisionId: $('#divisionSelector').find(":selected").val(),
                                                hotsos: selectedHotsosList,
                                            }
                                        }).success(function (json) {
                                            App.goUpper2Level();
                                        }).error(function (d) {
                                            console.log('error');
                                            console.log(d);
                                        });
                                        // }
                                    }


                                }).error(function (d) {
                                    console.log('error');
                                    console.log(d);
                                });

                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });
                            App.hideLoading();

                        }
                        //add mew message
                        else {

                            var selectedGenreList = '';
                            var selectedLanguageList = '';
                            var selectedSubtitleList = '';
									   if (App.boutiqueDetail.posterIndex[0] == 1)
										App.imageData = $('#imageContainer > img').cropper("getDataURL", { width:App.boutiqueDetail.posterWidth , height: App.boutiqueDetail.posterHeight }).replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
									else
										App.imageData = null;
									if (App.boutiqueDetail.posterIndex[1] == 1)
										App.imageData1 = $('#imageContainer1 > img').cropper("getDataURL", { width:App.boutiqueDetail.posterWidth , height: App.boutiqueDetail.posterHeight }).replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
									else
										App.imageData1 = null;		
									if (App.boutiqueDetail.posterIndex[2] == 1)
										App.imageData2 = $('#imageContainer2 > img').cropper("getDataURL", { width:App.boutiqueDetail.posterWidth , height: App.boutiqueDetail.posterHeight }).replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
									else
										App.imageData2 = null;
									if (App.boutiqueDetail.posterIndex[3] == 1)
										App.imageData3 = $('#imageContainer3 > img').cropper("getDataURL", { width:App.boutiqueDetail.posterWidth , height: App.boutiqueDetail.posterHeight }).replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
									else
										App.imageData3 = null;
                            $.ajax({
                                url: "api/boutique/uploadPhoto.php",
                                method: "POST",
                                dataType: "json",
                                data: {
									image: App.imageData,
									image1: App.imageData1,
									image2: App.imageData2,
									image3: App.imageData3
								}
                            }).success(function (json) {
                                console.log(json);

                                if (json.status == 502) {
                                    alert(App.strings.sessionTimeOut);
                                    location.reload();
                                    return;
                                }

								self.posterFilePath1[0] = json.data.filepath;
								self.posterFilePath1[1] = json.data.filepath1;
								self.posterFilePath1[2] = json.data.filepath2;
								self.posterFilePath1[3] = json.data.filepath3;


                                for (var x = 0; x < self.selected_genre.length; x++) {
                                    if (x != self.selected_genre.length - 1) {
                                        selectedGenreList += self.selected_genre[x] + ",";
                                    }
                                    else {
                                        selectedGenreList += self.selected_genre[x];
                                    }
                                }

                                for (var x = 0; x < self.selected_language.length; x++) {
                                    if (x != self.selected_language.length - 1) {
                                        selectedLanguageList += self.selected_language[x] + ",";
                                    }
                                    else {
                                        selectedLanguageList += self.selected_language[x];
                                    }
                                }

                                for (var x = 0; x < self.selected_subtitle.length; x++) {
                                    if (x != self.selected_subtitle.length - 1) {
                                        selectedSubtitleList += self.selected_subtitle[x] + ",";
                                    }
                                    else {
                                        selectedSubtitleList += self.selected_subtitle[x];
                                    }
                                }
                                $.ajax({
                                    url: "api/boutique/addProduct.php",
                                    method: "POST",
                                    dataType: "json",
                                    data: {
                                        categoryId: selectedGenreList,
                                        language: selectedLanguageList,
                                        subtitle: selectedSubtitleList,
                                        price: $("#priceInput").val(),
										remark: $("#remarkInput").val(),
                                        poster: self.posterFilePath1[0],
										poster1: self.posterFilePath1[1],
										poster2: self.posterFilePath1[2],
										poster3: self.posterFilePath1[3],
                                        rating: $("#ratingInput").val(),
                                        divisionId: $('#divisionSelector').find(":selected").val(),

                                        title_en: $('#detailTitle_en').val(),
                                        title_zh_hk: self.titleLangModel.zh_hk,
                                        title_zh_cn: self.titleLangModel.zh_cn,
                                        title_jp: self.titleLangModel.jp,
                                        title_fr: self.titleLangModel.fr,
                                        title_ar: self.titleLangModel.ar,
                                        title_es: self.titleLangModel.es,
                                        title_de: self.titleLangModel.de,
                                        title_ko: self.titleLangModel.ko,
                                        title_ru: self.titleLangModel.ru,
                                        title_pt: self.titleLangModel.pt,

                                        description_en: editor_en.getData(),
                                        description_zh_hk: self.desLangModel.zh_hk,
                                        description_zh_cn: self.desLangModel.zh_cn,
                                        description_jp: self.desLangModel.jp,
                                        description_fr: self.desLangModel.fr,
                                        description_ar: self.desLangModel.ar,
                                        description_es: self.desLangModel.es,
                                        description_de: self.desLangModel.de,
                                        description_ko: self.desLangModel.ko,
                                        description_ru: self.desLangModel.ru,
                                        description_pt: self.desLangModel.pt
                                    }
                                }).success(function (json) {
                                    if (json.status == 502) {
                                        alert(App.strings.sessionTimeOut);
                                        location.reload();
                                        return;
                                    }
                                    console.log(json);
                                    App.goUpperLevel();
                                }).error(function (d) {
                                    console.log(d);
                                });

                            }).error(function (d) {
                                console.log(d);
                                alert("Upload photo error, please try again.");
                                App.goUpperLevel();
                            });
                        }
                        App.yesNoPopup.destroy();
                    },
                    msg: "Are you sure to add/edit product?"
                }
            );


        });

        //handle language select box
        $("#lang_selectionBox").change(function () {

            var editor_lang = CKEDITOR.instances.editor2;

            if ($("#lang_selectionBox").val() != "") {
                self.currentLang = $("#lang_selectionBox").val();
            }

            //setup enable/disable the other language input box
            if ($("#lang_selectionBox").val() == 'en') {
                $('#container_lang input').attr('disabled', 'disabled');
                editor_lang.setReadOnly(true);
            }
            else {
                //alert("enable");
                $('#container_lang input').removeAttr('disabled');
                editor_lang.setReadOnly(false);
            }

            $('#container_lang input').val(eval("self.titleLangModel." + $("#lang_selectionBox").val()));

            var string = "";
            if (self.desLangModel.en.indexOf("<br />") == -1) {
                string = eval("self.desLangModel." + $("#lang_selectionBox").val()).replace(/\n/g, "<br />");
                //string  = string.replace(/\r/g, "<br />");
            }
            else {
                string = eval("self.desLangModel." + $("#lang_selectionBox").val());
            }
            //string = string.replace(/\r/g, "<br />");

            editor_lang.setData(string);
        });

        $('#container_lang input').blur(function () {
            var editor_lang = CKEDITOR.instances.editor2;
            //alert("hi");
            eval("self.titleLangModel." + $("#lang_selectionBox").val() + "= $('#container_lang input').val()");

            console.log(self.titleLangModel);
        });


        $("#takePictureField").on("change", self.gotPic);
		$("#takePictureField1").on("change", self.gotPic);
		$("#takePictureField2").on("change", self.gotPic);
		$("#takePictureField3").on("change", self.gotPic);

    },

    getSelectedGerne: function (_self) {
        var self = _self;
        var selected_genre = App.currentItem.genreId;
        if (selected_genre != null && selected_genre.length > 0) {
            self.selected_genre = selected_genre.split(',');
        } else {
            self.selected_genre[0] = null;
        }
    },

    getSelectedHotsos: function (_self) {
        var self = _self;
        self.selected_hotsos = App.currentItem.hotsos;
    },

    getSelectedLanguage: function (_self) {
        var self = _self;
        var selected_language = App.currentItem.languageId;
        if (selected_language != null && selected_language.length > 0) {
            self.selected_language = selected_language.split(',');
        } else {
            self.selected_language[0] = null;
        }
    },

    getSelectedSubtitle: function (_self) {
        var self = _self;
        var selected_subtitle = App.currentItem.subtitleId;
        if (selected_subtitle != null && selected_subtitle.length > 0) {
            self.selected_subtitle = selected_subtitle.split(',');
        } else {
            self.selected_subtitle[0] = null;
        }
    },

    getSelectedDivision: function (_self) {
        var self = _self;
        self.selected_division = App.currentItem.divisionId;
    },

    getRemark: function (_self) {
        var self = _self;
        self.remark = App.currentItem.remark;
    },
    getRating: function (_self) {
        var self = _self;
        self.rating = App.currentItem.rating;
    },

    initGenreList: function (_self) {
        var self = _self;

        $("#genre_list").empty();

        var str = "";
        for (var x = 0; x < self.genreList.length; x++) {
            var isChecked = false;
            for (var y = 0; y < self.selected_genre[y]; y++) {
                if (self.genreList[x].id == self.selected_genre[y]) {
                    isChecked = true;
                    break;
                }
            }

            if (isChecked) {
                str = str + "<label><input type='checkbox' id=genreCheckbox" + x + " value='" + self.genreList[x].id + "' order=" + x + " checked />" + self.genreList[x].en + "</label>";
            } else {
                str = str + "<label><input type='checkbox' id=genreCheckbox" + x + " value='" + self.genreList[x].id + "' order=" + x + " />" + self.genreList[x].en + "</label>";
            }
        }

        $("#genre_list").html(str);
        self.initGenreCheckBox(self, $("#genre_list"));
    },

    initHotsosList: function (_self) {
        var self = _self;

        $("#hotsos_list").empty();

        var str = "";
        for (var x = 0; x < self.hotsosList.length; x++) {
            if (self.hotsosList[x].id == self.selected_hotsos) {
                str = str + "<label><input type='radio' name='hotsos' id=hotsosCheckbox" + x + " value='" + self.hotsosList[x].id + "' order=" + x + " checked />" + self.hotsosList[x].name + "</label>";
            } else {
                str = str + "<label><input type='radio' name='hotsos' id=hotsosCheckbox" + x + " value='" + self.hotsosList[x].id + "' order=" + x + " />" + self.hotsosList[x].name + "</label>";
            }
        }

        $("#hotsos_list").html(str);
        self.initHotsosCheckBox(self, $("#hotsos_list"));
    },

    initLanguageList: function (_self) {
        var self = _self;

        $("#language_list").empty();

        var str = "";
        for (var x = 0; x < self.languageList.length; x++) {
            var isChecked = false;

            for (var y = 0; y < self.selected_language[y]; y++) {
                if (self.languageList[x].id == self.selected_language[y]) {
                    isChecked = true;
                    break;
                }
            }

            if (isChecked) {
                str = str + "<label><input type='checkbox' id=languageCheckbox" + x + " value='" + self.languageList[x].id + "' order=" + x + " checked />" + self.languageList[x].en + "</label>";
            } else {
                str = str + "<label><input type='checkbox' id=languageCheckbox" + x + " value='" + self.languageList[x].id + "' order=" + x + " />" + self.languageList[x].en + "</label>";
            }
        }

        $("#language_list").html(str);
        self.initLanguageCheckBox(self, $("#language_list"));
    },


    initSubtitleList: function (_self) {
        var self = _self;

        $("#subtitle_list").empty();

        var str = "";
        for (var x = 0; x < self.subtitleList.length; x++) {
            var isChecked = false;

            for (var y = 0; y < self.selected_subtitle[y]; y++) {
                if (self.subtitleList[x].id == self.selected_subtitle[y]) {
                    isChecked = true;
                    break;
                }
            }

            if (isChecked) {
                str = str + "<label><input type='checkbox' id=subtitleCheckbox" + x + " value='" + self.subtitleList[x].id + "' order=" + x + " checked />" + self.subtitleList[x].en + "</label>";
            } else {
                str = str + "<label><input type='checkbox' id=subtitleCheckbox" + x + " value='" + self.subtitleList[x].id + "' order=" + x + " />" + self.subtitleList[x].en + "</label>";
            }
        }

        $("#subtitle_list").html(str);
        self.initSubtitleCheckBox(self, $("#subtitle_list"));
    },

    initDivisionList: function (_self) {
        var self = _self;

        $("#division_list").empty();

        var str = "<select id=\"divisionSelector\">";

        str = str + "<option value=''>--</option>";

        for (var x = 0; x < self.divisionList.length; x++) {

            if (self.divisionList[x].id == self.selected_division) {
                str = str + "<option id=division" + x + " value='" + self.divisionList[x].id + "' order=" + x + " selected >" + self.divisionList[x].en + "</option>";
            } else {
                str = str + "<option id=division" + x + " value='" + self.divisionList[x].id + "' order=" + x + " >" + self.divisionList[x].en + "</option>";
            }
        }
        str = str + "<\select>";
        $("#division_list").html(str);
    },
    initRemark: function (_self) {
        var self = _self;
        $("#remarkInput").val(self.remark);
    },
    initRating: function (_self) {
        var self = _self;
        $("#ratingInput").val(self.rating);
    },
    initPrice: function (_self) {
        var self = _self;
        $("#priceInput").val(self.price);
    },

    saveDescriptionLang: function () {
        var editor_lang = CKEDITOR.instances.editor2;
        eval("this.desLangModel." + $("#lang_selectionBox").val() + "= editor_lang.getData()");
    },
    close: function () {
        console.log("close fire");
    },
    destroy: function () {

        //COMPLETELY UNBIND THE VIEW

        this.selectGuestModel = null;
        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    setCropper: function(){

        if (App.boutiqueDetail.index == 0) {
			$('#imageContainer > img').cropper({
				aspectRatio:   App.boutiqueDetail.posterWidth /  App.boutiqueDetail.posterHeight,
				autoCropArea:1.0 ,
				guides: true,
				highlight: false,
				dragCrop: false,
				movable: false,
				resizable: false
			});
		} else if (App.boutiqueDetail.index == 1) {
			$('#imageContainer1 > img').cropper({
				aspectRatio:   App.boutiqueDetail.posterWidth /  App.boutiqueDetail.posterHeight,
				autoCropArea:1.0 ,
				guides: true,
				highlight: false,
				dragCrop: false,
				movable: false,
				resizable: false
			});
		} else if (App.boutiqueDetail.index == 2) {
			$('#imageContainer2 > img').cropper({
				aspectRatio:   App.boutiqueDetail.posterWidth /  App.boutiqueDetail.posterHeight,
				autoCropArea:1.0 ,
				guides: true,
				highlight: false,
				dragCrop: false,
				movable: false,
				resizable: false
			});
		} else if (App.boutiqueDetail.index == 3) {
			$('#imageContainer3 > img').cropper({
				aspectRatio:   App.boutiqueDetail.posterWidth /  App.boutiqueDetail.posterHeight,
				autoCropArea:1.0 ,
				guides: true,
				highlight: false,
				dragCrop: false,
				movable: false,
				resizable: false
			});
		}
    },
    gotPic: function (event) {

        var self = this;
		var indexOnLoad = 0;
		var flag = false;
		if (event.target.id == "takePictureField") {
			App.boutiqueDetail.posterIndex[0] = 1;
			App.boutiqueDetail.index = 0;
		} else if (event.target.id == "takePictureField1") {
			App.boutiqueDetail.posterIndex[1] = 1;
			App.boutiqueDetail.index = 1;
		} else if (event.target.id == "takePictureField2") {
			App.boutiqueDetail.posterIndex[2] = 1;
			App.boutiqueDetail.index = 2;
		} else if (event.target.id == "takePictureField3") {
			App.boutiqueDetail.posterIndex[3] = 1;
			App.boutiqueDetail.index = 3;
		}
        if (event.target.files.length == 1 && event.target.files[0].type.indexOf("image/") == 0) {

		    flag = true;
            App.boutiqueDetail.isNewPoster = true;
            

            //get only filename without extension
            var file = event.target.files[0];
            var tempFileComponents = file.type.split("/");
            var fileExtension = tempFileComponents[1];
            App.imageFileName = file.name.substring(0, file.name.length - fileExtension.length - 1);
            App.imageFileName = App.imageFileName.replace(/\s+/g, '');

            console.log('File size =' + file.size);
            console.log('max size =' + App.boutiqueDetail.uploadImageSizeLimit * 1024 * 1024);
            //image file exceed upper limited size, not allow to pass
            if (file.size > App.boutiqueDetail.uploadImageSizeLimit * 1024 * 1024) {
                $("#takePictureField").val('');
                alert("File size is too big. Please upload image less than " + App.boutiqueDetail.uploadImageSizeLimit + "mb.");
                return;
            }  else{
                //allow to remove previous cropped component.
                if (App.boutiqueDetail.index == 0 && App.imageData)
					$('#imageContainer > img').cropper("destroy");
                if (App.boutiqueDetail.index == 1 && App.imageData1)
					$('#imageContainer1 > img').cropper("destroy");
                if (App.boutiqueDetail.index == 2 && App.imageData2)
					$('#imageContainer2 > img').cropper("destroy");
                if (App.boutiqueDetail.index == 3 && App.imageData3)
					$('#imageContainer3 > img').cropper("destroy");				
            }

            var url = window.URL ? window.URL : window.webkitURL;

			if (App.boutiqueDetail.index == 0) {
				$('#uploadImage').empty();
				$('#uploadImage').attr('src', url.createObjectURL(event.target.files[0]));				
			} else if (App.boutiqueDetail.index == 1) {
				$('#uploadImage1').empty();
				$('#uploadImage1').attr('src', url.createObjectURL(event.target.files[0]));				
			} else if (App.boutiqueDetail.index == 2) {
				$('#uploadImage2').empty();
				$('#uploadImage2').attr('src', url.createObjectURL(event.target.files[0]));				
			} else if (App.boutiqueDetail.index == 3) {
				$('#uploadImage3').empty();
				$('#uploadImage3').attr('src', url.createObjectURL(event.target.files[0]));				
			}
            var canvasImage = document.createElement("img");
            canvasImage.src = url.createObjectURL(event.target.files[0]);

			if (App.boutiqueDetail.index == 0) {
				$("#imageContainer").show();
			} else if (App.boutiqueDetail.index == 1) {
				$("#imageContainer1").css('margin-top',6.5);
				$("#imageContainer1").height(400)
				$("#imageContainer1").show();
			} else if (App.boutiqueDetail.index == 2) {
				$("#imageContainer2").css('margin-top',6.5);
				$("#imageContainer2").height(400)
				$("#imageContainer2").show();
			} else if (App.boutiqueDetail.index == 3) {
				$("#imageContainer3").css('margin-top',6.5);
				$("#imageContainer3").height(400)
				$("#imageContainer3").show();
			}


            canvasImage.onload = function () {
                console.log("Image onLoad fire");
                //change image data to base64
                var canvas = document.createElement('CANVAS'),
                    ctx = canvas.getContext('2d'),
                    outputFormat = "image/jpeg";

                canvas.height = canvasImage.height;
                canvas.width = canvasImage.width;
                ctx.drawImage(canvasImage, 0, 0);
				if (App.boutiqueDetail.index == 0) {
					//alert("hi0:"+canvas.height+":"+canvas.width);
					App.imageData = canvas.toDataURL(outputFormat);
					App.imageData = App.imageData.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
				} else if (App.boutiqueDetail.index == 1) {
					//alert("hi1:"+canvas.height+":"+canvas.width);
					App.imageData1 = canvas.toDataURL(outputFormat);
					App.imageData1 = App.imageData1.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");					
				} else if (App.boutiqueDetail.index == 2) {
					//alert("hi1:"+canvas.height+":"+canvas.width);
					App.imageData2 = canvas.toDataURL(outputFormat);
					App.imageData2 = App.imageData2.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");					
				} else if (App.boutiqueDetail.index == 3) {
					//alert("hi1:"+canvas.height+":"+canvas.width);
					App.imageData3 = canvas.toDataURL(outputFormat);
					App.imageData3 = App.imageData3.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");					
				}

                canvas = null;

                App.boutiqueDetail.setCropper();
				indexOnLoad++;
            };
        } else {
            alert("Please select an image file.");
            $('#uploadImage').val("");
        }
    },

    loadPhoto: function () {
        var self = this;
        if (self.posterFilePath1[0] != null && !self.posterFilePath1[0].isEmpty) {
            var srcPath = "/" + App.baseFolder + "/" + self.posterFilePath1[0];
            $('#uploadImage').empty();
            $('#uploadImage').attr('src', srcPath);
            $("#imageContainer").show();
        }
        if (self.posterFilePath1[1] != null && self.posterFilePath1[1]!="") {
            var srcPath = "/" + App.baseFolder + "/" + self.posterFilePath1[1];
            $('#uploadImage1').empty();
            $('#uploadImage1').attr('src', srcPath);
			$("#imageContainer1").css('margin-top',6);
			$("#imageContainer1").height(400);
            $("#imageContainer1").show();
        }
        if (self.posterFilePath1[2] != null && self.posterFilePath1[2]!="") {
            var srcPath = "/" + App.baseFolder + "/" + self.posterFilePath1[2];
            $('#uploadImage2').empty();
            $('#uploadImage2').attr('src', srcPath);
			$("#imageContainer2").css('margin-top',6);
			$("#imageContainer2").height(400);
            $("#imageContainer2").show();
        }
        if (self.posterFilePath1[3] != null && self.posterFilePath1[3]!="") {
            var srcPath = "/" + App.baseFolder + "/" + self.posterFilePath1[3];
            $('#uploadImage3').empty();
            $('#uploadImage3').attr('src', srcPath);
			$("#imageContainer3").css('margin-top',6);
			$("#imageContainer3").height(400);
            $("#imageContainer3").show();
        }
    },

    loadPrice: function () {
        var self = this;
        if (self.price != null) {
            priceInput.price = self.price;
        }
    },

    loadRemark: function () {
        var self = this;
        if (self.remark != null) {
            remarkInput.remark = self.remark;
        }
    },

    initCheckBoxStyle: function (field) {
        jQuery.fn.multiselect = function () {
            $(this).each(function () {
                var checkboxes = $(this).find("input:checkbox");
                checkboxes.each(function () {
                    var checkbox = $(this);
                    // Highlight pre-selected checkboxes
                    if (checkbox.prop("checked"))
                        checkbox.parent().addClass("multiselect-on");

                    // Highlight checkboxes that the user selects
                    checkbox.click(function () {
                        if (checkbox.prop("checked"))
                            checkbox.parent().addClass("multiselect-on");
                        else
                            checkbox.parent().removeClass("multiselect-on");
                    });
                });
            });
        };

        $(function () {
            field.multiselect();
        });
    },

    initGenreCheckBox: function (_self, field) {
        var self = _self;

        for (var x = 0; x < self.genreList.length; x++) {
            $("#genreCheckbox" + x).click(function () {
                var index = $(this).attr("order");
                var isExist = false;
                var existIndex;


                for (var y = 0; y < self.selected_genre.length; y++) {

                    console.log(self.genreList.length + "|" + index);
                    if (self.selected_genre[y] == self.genreList[index].id) {
                        isExist = true;
                        existIndex = y;
                    }
                }

                if ($(this).is(":checked")) {
                    if (!isExist) {
                        if(self.selected_genre[0] == null){
                            self.selected_genre[0] = self.genreList[index].id
                        } else {
                            self.selected_genre.push(self.genreList[index].id);
                        }
                    }
                } else {
                    if (isExist) {
                        self.selected_genre.splice(existIndex, 1);
                    }
                }
            });
        }
        self.initCheckBoxStyle(field);
    },
    initHotsosCheckBox: function (_self, field) {
        var self = _self;

        for (var x = 0; x < self.hotsosList.length; x++) {
            $("#hotsosCheckbox" + x).click(function () {
                 var index = $(this).attr("order");
                 self.selected_hotsos = self.hotsosList[index].id;
            });
        }
    },
    initLanguageCheckBox: function (_self, field) {
        var self = _self;

        for (var x = 0; x < self.languageList.length; x++) {
            $("#languageCheckbox" + x).click(function () {
                var index = $(this).attr("order");
                var isExist = false;
                var existIndex;

                for (var y = 0; y < self.selected_language.length; y++) {
                    if (self.selected_language[y] == self.languageList[index].id) {
                        isExist = true;
                        existIndex = y;
                    }
                }

                if ($(this).is(":checked")) {
                    if (!isExist) {
                        if(self.selected_language[0] == null) {
                            self.selected_language[0] = self.languageList[index].id;
                        } else {
                            self.selected_language.push(self.languageList[index].id);
                        }
                    }
                } else {
                    if (isExist) {
                        self.selected_language.splice(existIndex, 1);
                    }
                }
            });
        }
        self.initCheckBoxStyle(field);
    },
    initSubtitleCheckBox: function (_self, field) {
        var self = _self;

        for (var x = 0; x < self.subtitleList.length; x++) {
            $("#subtitleCheckbox" + x).click(function () {
                var index = $(this).attr("order");
                var isExist = false;
                var existIndex;

                for (var y = 0; y < self.selected_subtitle.length; y++) {
                    if (self.selected_subtitle[y] == self.subtitleList[index].id) {
                        isExist = true;
                        existIndex = y;
                    }
                }

                if ($(this).is(":checked")) {
                    if (!isExist) {
                        if(self.selected_subtitle[0] == null){
                            self.selected_subtitle[0] = self.subtitleList[index].id;
                        } else {
                            self.selected_subtitle.push(self.subtitleList[index].id);
                        }
                    }
                } else {
                    if (isExist) {
                        self.selected_subtitle.splice(existIndex, 1);
                    }
                }
            });
        }
        self.initCheckBoxStyle(field);
    },

    isHide: false
});
