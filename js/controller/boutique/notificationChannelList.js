App.NotificationChannel = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    initialize: function(options){
        if(options && options.listTitle){
            this.title = options.listTitle;
        } else {
            this.title = "Notification";
        }
        this.render();
    },
    events: {
    },

    render: function(){

        var self = this;
        $.ajax({
            url : "php/html/itemList.php",
            method : "GET",
            dataType: "html",
        }).success(function(html){

            $(self.el).append(html)
                .promise()
                .done(function(){
                    self.postUISetup();
                });


        }).error(function(d){
            console.log('error');
            console.log(d);
        });

    },
    postUISetup: function() {
        var self = this;

        $("#search").hide();
        $("#item_list_head_title").text(self.title);

        $('#addBtn').on('click',function(){
            var newData = {
                name: "",
                hotsos: "",
                add: true,
            };
            self.data.push(newData);
            self.updateItem();
        });
        var addBtnText = self.title;
        $('#addBtn').text("+Add " + addBtnText);

        self.reloadTable();
    },
    reloadTable: function() {
        var self = this;

        self.emptyTable();

        $.ajax({
            url : "api/boutique/getNotificationChannel.php",
            method : "GET",
            cache: false,
            dataType: "json"
        }).success(function(json){
            if (typeof json.data !== "undefined") {
                self.data = [];
                for (var i = 0; i < json.data.length; i++) {
                    var tempData = json.data[i];
                    tempData["add"] = false;
                    self.data.push(tempData);
                }
            }
            self.updateItem();
        }).error(function(d){
            console.log('error');
            console.log(d);
        });
    },
    emptyTable: function() {
        $("#resultTable").empty();
        var tableHeaderString = "<th class='tableHead'>Channel Name</th>"+
            "<th class='tableHead'>HOTSOS Code</th>"+
            "<th class='tableHead'></th>";
        $("#resultTable").append(
            "<tbody><tr>"+
            tableHeaderString+
            "</tr><tbody>");
    },
    updateItem: function() {
        var self = this;

        self.emptyTable();

        for (var i = 0; i < self.data.length; i++) {
            var tempData = self.data[i];

            var content = "";

            if (tempData.add) {
                content += "<td><input type='text' id='name-" + i + "'></input></td>";
            } else {
                content += "<td><div class='titleText' id='name-" + i + "'>" + tempData.name + "</div></td>";
            }

            content += "<td><input type='text' id='hotsos-" + i + "' value='" + tempData.hotsos + "'></input></td>";

            content += "<td>";
            content += "<span id='submit-" + i + "' class='notification-submit' title='click to submit'><button type='button' class='btn btn-default'>Submit</button></span>";
            if (!tempData.add) {
                content += "<span id='delete-" + i + "' class='notification-delete' title='click to delete'><button type='button' class='btn bg-maroon'>Delete</button></span>";
            }
            content += "</td>";

            $("#resultTable").append("<tr>" + content + "</tr>");

            $("#submit-" + i).attr("index", i);
            $("#submit-" + i).on('click', function() {
                var index = $(this).attr("index");
                var tempData = self.data[index];
                var postData = {};
                if (tempData.add) {
                    postData = {
                        name: $("#name-" + index).val(),
                        hotsos: $("#hotsos-" + index).val()
                    };
                } else {
                    postData = {
                        id: tempData.id,
                        hotsos: $("#hotsos-" + index).val()
                    };
                }

                $.ajax({
                    url: "api/boutique/updateNotificationChannel.php",
                    method: "POST",
                    dataType: "json",
                    data: postData
                }).success(function(json){
                    self.reloadTable();
                }).error(function(d){
                    console.log('error');
                    console.log(d);
                });

            });
            if (!tempData.add) {
                $("#delete-" + i).attr("index", i);
                $("#delete-" + i).on('click', function() {
                    var index = $(this).attr("index");
                    var tempData = self.data[index];
                    var postData = {
                        id: tempData.id,
                    };
                    App.yesNoPopup = new App.YesNoPopup(
                        {
                            yesFunc: function () {
                                $.ajax({
                                    url: "api/boutique/deleteNotificationChannel.php",
                                    method: "POST",
                                    dataType: "json",
                                    data: postData
                                }).success(function(json){
                                    App.yesNoPopup.destroy();
                                    self.reloadTable();
                                }).error(function(d){
                                    console.log('error');
                                    console.log(d);
                                });
                            },
                            msg: "Are you sure to delete this channel?"
                        }
                    );
                });
            }
        }
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW


        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    refresh: function(){
        var self = this;
        $(self.el).empty();
        self.render();
    },
    isHide : false
});
