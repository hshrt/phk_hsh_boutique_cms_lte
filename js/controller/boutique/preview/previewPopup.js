App.PreviewPopup = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    itemTitle: "",
    parentTitle: "",
    itemImageLink: "",
    description: "",
    showTypeBox:true,
    img1: "",
    img2: "",
    img3: "",
    img4: "",
    catText: "",
    price: "",
    parentId: 0,
    // It's the first function called when this view it's instantiated.
    initialize: function(options){
        if(options && options.previewType){
            this.previewType = options.previewType;
        }
        if(options && options.itemTitle){
            this.itemTitle = options.itemTitle;
        }
        if(options && options.parentTitle){
            this.parentTitle = options.parentTitle;
        }
        if(options && options.itemImageLink){
            this.itemImageLink = options.itemImageLink;
        }
        if(options && options.description){
            this.description = options.description;
        }
        if(options && options.price){
            this.price = options.price;
        }
        if(options && options.lang){
            this.lang = options.lang;
        }
        
        
        var myPath=window.location.pathname;
        
        if(options && options.img1){
            this.img1 = options.img1.replace('/' + myPath + '/','');
        }
        if(options && options.img2){
            this.img2 = options.img2.replace('/' + myPath + '/','');
        }
        if(options && options.img3){
            this.img3 = options.img3.replace('/' + myPath + '/','');
        }
        if(options && options.img4){
            this.img4 = options.img4.replace('/' + myPath + '/','');
        }
        
        if(options && options.catText){
            this.catText = options.catText;
        }
        

        this.render();
    },
    events: {

        'click #cancel_btn'  : 'destroy'
    },
    setupUIHandler : function(){

    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){
        //alert($(window).width());

        var self = this;
        console.log("render createItemPopup");
        $.ajax({
         url : "php/html/preview/previewPopup.php",
         method : "POST",
         dataType: "html",
         data : {}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
         }).success(function(html){
             console.log(html);
            $('#container').append(html).
                promise()
                .done(function(){

                    $('.popup_box_container').show(true);

                    var baseImageURL;
                    if(self.previewType == "generalItem"){
                        baseImageURL = "images/general_item_base.png";
                    }
                    else if(self.previewType == "inRoomDinningSubcat"){
                        baseImageURL = "images/in_room_dinning_subcat.png";
                    }
                    else if(self.previewType == "inRoomDinningFood"){
                        baseImageURL = "images/in_room_dinning_food.png";
                    }
                    else if(self.previewType == "boutique"){
                        baseImageURL = "images/boutique.png";
                    }

                    $("#appBG").css({"background-image":"url('"+baseImageURL+"')"});
                    //$("#appBG").css({"top":"-102px"});
                    //$("#appBG").css({"left":"-40px"});
                    //$("#appBG").css({"width":"880px"});
                    //$("#appBG").css({"height":"655px"});

                    $("#closeBtn").on('click',function(){
                        console.log("closeBtn clicked");
                        self.destroy();
                    });
                    
                    //console.log(self.img1);
                    
                    $("#img1").css({"background-image":"url('"+self.img1+"')"});
                    $("#img1").on('click', function () {
                      $("#img1").hide();
                      $("#img2").show();
                      $("#img3").hide();
                      $("#img4").hide();
                    });
                    
                    
                    
                    console.log(self.img2);
                    
                    if ( self.img2 != "" ) 
                      $("#img2").css({"background-image":"url('"+self.img2+"')"});
                      
                    $("#img2").on('click', function () {
                      $("#img1").hide();
                      $("#img2").hide();
                      $("#img3").show();
                      $("#img4").hide();
                    });
                      
                    if ( self.img3 != "" ) 
                      $("#img3").css({"background-image":"url('"+self.img3+"')"});
                      
                    $("#img3").on('click', function () {
                      $("#img1").hide();
                      $("#img2").hide();
                      $("#img3").hide();
                      $("#img4").show();
                    });
                    
                    if ( self.img4 != "" ) 
                      $("#img4").css({"background-image":"url('"+self.img4+"')"});
                      
                    $("#img4").on('click', function () {
                      $("#img1").show();
                      $("#img2").hide();
                      $("#img3").hide();
                      $("#img4").hide();
                    });
                    
                    //$("#catText").html("Category: " + self.catText);
                    
                    //$("#highlightText").html("Highlights:<br>" + self.itemTitle);

                    //$("#itemTitleText").text(self.itemTitle.toUpperCase());
                    //ellipsis function for the description text
                    $("#itemTitleText").dotdotdot({ellipsis	: '... ',height: 50,wrap: 'word',callback	: function( isTruncated, orgContent ) {if(!isTruncated){console.log("no truncated"); $("#itemTitleText").css("height", "auto");}},watch: "window"});

                    $("#itemImage").css({"background-image":"url('"+self.itemImageLink+"')"});
                    
                    $("#description_text").html("<div style='color:#BEBEBE; font-size:20px' >" + self.itemTitle + "</div><div style='color:#BEBEBE; font-size:11px' >" + "Category: " + self.catText + "<br>Price: HKD " + self.price + "</div><div style='color:#FFFFFF; font-size:12px' ><br>Description:<br>" + self.description+"</div>");
                    

                    $("#imageOption").change(function() {
                        if(this.checked) {
                            $("#itemImage").show();
                        }
                        else{
                            $("#itemImage").hide();
                        }
                    });

                    $("#descriptionOption").change(function() {
                        if(this.checked) {
                            $("#description_text").show();
                            $("#overlayBox").show();
                            $("#arrowBtn").show();
                            $("#zoom_container").show();
                        }
                        else{
                            $("#description_text").hide();
                            $("#overlayBox").hide();
                            $("#arrowBtn").hide();
                            $("#zoom_container").hide();
                        }
                    });
                    $("#highlightOption").change(function() {
                        if(this.checked) {
                            $("#overlayBox1").show();
                            $("#overlayBox2").show();
                            $("#overlayBox3").show();
                            $("#overlayBox4").show();
                            $("#overlayBox5").show();
                            $("#overlayBox6").show();
                        }
                        else{
                            $("#overlayBox1").hide();
                            $("#overlayBox2").hide();
                            $("#overlayBox3").hide();
                            $("#overlayBox4").hide();
                            $("#overlayBox5").hide();
                            $("#overlayBox6").hide();
                        }
                    });

                    if(self.lang == "ar"){
                        $("#description_text").css("direction", "rtl");
                    }

                    //$("#dimmer").click(function(e){e.preventDefault();});


                });
                
                
                

         }).error(function(d){
            console.log('error');
            console.log(d);
         });
    },

    clickConfirm : function(){

    },

    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW
        //this.undelegateEvents();
        //this.$el.removeData().unbind();
        $(".popup_box_container").remove();
        this.undelegateEvents();
        //Backbone.View.prototype.remove.call(this);
        //Remove view from DOM
        //this.remove();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});