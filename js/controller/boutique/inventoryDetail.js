App.InventoryDetail = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.

    uploadImageSizeLimit: 5,
    posterWidth: 240,
    posterHeight: 360,

    currentLang: "en",
    jsonObj: null,

    type: null,
    movieTitleId: null,
    titleLangModel: {},
    desLangModel: {},

    genreList: [],
    selected_genre: null,

    languageList: [],
    selected_language: [],

    subtitleList: [],
    selected_subtitle: [],

    posterFilePath: null,
    stock_previous: null,
    stock: null,
    year: null,

    isNewPoster: false,

    title: "Movie",

    initialize: function (options) {
        this.selectGuestModel = [];
        if (options && options.id) {
            this.movieTitleId = options.id;
        }
        if (options && options.type) {
            this.type = options.type;
        }
        this.render();
    },
    events: {
        'click #closeBtn_start': 'backToIndex'
    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function () {

        var self = this;

        //remove any cached data if it is for new message
        if (self.type == "new") {
            App.currentItem = [];
            App.currentId = "";
            self.selected_genre = null;
            self.selected_language = [];
            self.selected_subtitle = [];
            self.titleLangModel = [];
            self.desLangModel = [];
        } else {
            App.currentItem = [];
            App.currentId = this.movieTitleId;
        }

        $.ajax({
            url: "php/html/inventoryDetail.php",
            method: "GET",
            dataType: "html",
            data: {}
        }).success(function (html) {
            $(self.el).append(html).promise()
                .done(function () {

                    CKEDITOR.replace('editor1');

                    CKEDITOR.replace('editor2', {
                        on: {
                            focus: function () {
                                console.log("focus editor2")
                            },
                            blur: function () {
                                self.saveDescriptionLang()
                            }
                        }
                    });


                    //this is editing existed message
                    if (self.type != "new") {

                        $.ajax({
                            url: "api/boutique/getMovie.php",
                            method: "GET",
                            dataType: "json",
                            data: {movieId: App.currentId, type: 'cms'}
                        }).success(function (json) {

                            App.currentItem = json.data[0];

                            self.getSelectedGerne(self);
                            self.getSelectedLanguage(self);
                            self.getSelectedSubtitle(self);
                            self.posterFilePath = App.currentItem.poster;
                            self.stock = App.currentItem.stock;
                            self.year = App.currentItem.year;

                            self.setupUIHandler();

                            //get language info for item Title
                            $.ajax({
                                url: "api/boutique/getLanguageMap.php",
                                method: "POST",
                                dataType: "json",
                                data: {id: App.currentItem.titleId}
                            }).success(function (json) {
                                console.log(json.data[0]);
                                self.titleLangModel = json.data[0];
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

                            //get language info for item Description
                            $.ajax({
                                url: "api/boutique/getLanguageMap.php",
                                method: "POST",
                                dataType: "json",
                                data: {id: App.currentItem.descriptionId}
                            }).success(function (json) {
                                console.log(json.data[0]);
                                self.desLangModel = json.data[0];
                                console.log("ding = " + self.desLangModel.zh_hk);
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

                            //get genre list
                            $.ajax({
                                url: "api/boutique/getGenreList.php",
                                method: "GET",
                                dataType: "json",
                                data: {}
                            }).success(function (json) {
                                self.genreList = json.data;
                                self.initGenreList(self);
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

                            //get language list
                            $.ajax({
                                url: "api/boutique/getLanguageList.php",
                                method: "GET",
                                dataType: "json",
                                data: {}
                            }).success(function (json) {
                                self.languageList = json.data;
                                self.initLanguageList(self);
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

                            //get subtitle list
                            $.ajax({
                                url: "api/boutique/getSubtitleList.php",
                                method: "GET",
                                dataType: "json",
                                data: {}
                            }).success(function (json) {
                                self.subtitleList = json.data;
                                self.initSubtitleList(self);
                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });

                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });
                    }
                    else {//this is for new message
                        self.setupUIHandler();

                        //get genre list
                        $.ajax({
                            url: "api/boutique/getGenreList.php",
                            method: "GET",
                            dataType: "json",
                            data: {}
                        }).success(function (json) {
                            self.genreList = json.data;
                            self.initGenreList(self);
                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });

                        //get language list
                        $.ajax({
                            url: "api/boutique/getLanguageList.php",
                            method: "GET",
                            dataType: "json",
                            data: {}
                        }).success(function (json) {
                            self.languageList = json.data;
                            self.initLanguageList(self);
                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });

                        //get subtitle list
                        $.ajax({
                            url: "api/boutique/getSubtitleList.php",
                            method: "GET",
                            dataType: "json",
                            data: {}
                        }).success(function (json) {
                            self.subtitleList = json.data;
                            self.initSubtitleList(self);
                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });
                    }

                    setTimeout(function () {
                        var editor2 = CKEDITOR.instances.editor2;
                        editor2.setReadOnly(true);
                    }, 1000);

                });

        }).error(function (d) {
            console.log('error');
            console.log(d);
        });


    },
    showPopup: function () {
        var self = this;
        //alert(self.title);
        App.createItemPopup = new App.CreateItemPopup(
            {

                title: self.title == "Library" ? "Page" : self.title,
                parentId: self.parentId
            }
        );
    },
    backToIndex: function () {
        window.parent.PeriEvent.backToIndex();
    },
    moveToTheme: function () {
        //this.destroyItem();
        $(this.el).hide();
        $("#blackTemp").show();
        $("#content").show();
    },
    showUp: function () {
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    setupUIHandler: function () {
        var self = this;

        //pre-fill the form
        self.type != "new" ? $('#itemName').text("Edit Movie") : $('#itemName').text("New Movie");

        if (self.type != "new") {
            self.loadPhoto();
            self.loadStock();
            self.loadYear();
        }

        $('#container_en input').val(App.currentItem.movieTitle);
        $('#container_lang input').val(App.currentItem.movieTitle);

        var editor_en = CKEDITOR.instances.editor1;
        var editor_lang = CKEDITOR.instances.editor2;

        setTimeout(function () {
            var string = "";

            if (self.desLangModel.en.indexOf("<br />") == -1 && App.currentItem.lastUpdateBy == "TC3") {
                string = self.desLangModel.en.replace(/\n/g, "<br />");
            }
            else {
                string = self.desLangModel.en;
            }
            editor_en.setData(string);
        }, 1000);


        if (self.type == "new" || App.currentItem.available != self.stock) {
            $("#delete_btn").hide();
        }

        //handle delete button
        $('#delete_btn').on('click', function () {
            App.yesNoPopup = new App.YesNoPopup(
                {
                    yesFunc: function () {
                        $.ajax({
                            url: "api/boutique/deleteProduct.php",
                            method: "POST",
                            dataType: "json",
                            data: {id: App.currentId}
                        }).success(function (json) {
                            if (json.status == 502) {
                                alert(App.strings.sessionTimeOut);
                                location.reload();
                                return;
                            }
                            App.yesNoPopup.destroy();
                            App.goUpper2Level();

                        }).error(function (d) {
                            console.log('error');
                            console.log(d);
                        });
                    },
                    msg: "Are you sure to delete this product?"
                }
            );
        });

        //handle save button
        $('#save_btn').on('click', function () {

            if (self.titleLangModel != null && self.titleLangModel.zh_cn != null && self.titleLangModel.zh_cn.length < 1) {
                alert("Please input CN Title .");
                return;
            }

            if (self.desLangModel != null && self.desLangModel.zh_cn != null && self.desLangModel.zh_cn.length < 1) {
                alert("Please input CN Description .");
                return;
            }

            if (($("#yearInput").val()) == null || ($("#yearInput").val()).length < 1) {
                alert("Please fill in Year.");
                return;
            }

            if (self.selected_genre == null) {
                alert("Please select Genre.");
                return;
            }

            if (self.selected_language[0] == null) {
                alert("Please select at least one language.");
                return;
            }

            if (self.selected_subtitle[0] == null) {
                alert("Please select at least one subtitle.");
                return;
            }

            if (self.type == "new") {
                if (!App.imageData || !App.imageFileName) {
                    alert("Please upload a poster first.");
                    return;
                }
            }

            App.yesNoPopup = new App.YesNoPopup(
                {
                    yesFunc: function () {
                        App.showLoading();

                        //save the description for other language first
                        self.saveDescriptionLang();

                        //updateItem exist message
                        if (self.type != "new") {
                            //updateItem Title of Item

                            var selectedLanguageList = '';
                            var selectedSubtitleList = '';

                            $.ajax({
                                url: "api/boutique/updateDict.php",
                                method: "POST",
                                dataType: "text",
                                data: {
                                    title_id: App.currentItem.titleId,
                                    title_en: $('#detailTitle_en').val(),
                                    title_zh_hk: self.titleLangModel.zh_hk,
                                    title_zh_cn: self.titleLangModel.zh_cn,
                                    title_jp: self.titleLangModel.jp,
                                    title_fr: self.titleLangModel.fr,
                                    title_ar: self.titleLangModel.ar,
                                    title_es: self.titleLangModel.es,
                                    title_de: self.titleLangModel.de,
                                    title_ko: self.titleLangModel.ko,
                                    title_ru: self.titleLangModel.ru,
                                    title_pt: self.titleLangModel.pt
                                }
                            }).success(function (json) {

                                console.log(json);
                                if (json.status == 502) {
                                    alert(App.strings.sessionTimeOut);
                                    location.reload();
                                    return;
                                }

                                //updateItem Description of Item
                                $.ajax({
                                    url: "api/boutique/updateDict.php",
                                    method: "POST",
                                    dataType: "text",
                                    data: {
                                        title_id: App.currentItem.descriptionId,
                                        title_en: editor_en.getData(),
                                        title_zh_hk: self.desLangModel.zh_hk,
                                        title_zh_cn: self.desLangModel.zh_cn,
                                        title_jp: self.desLangModel.jp,
                                        title_fr: self.desLangModel.fr,
                                        title_ar: self.desLangModel.ar,
                                        title_es: self.desLangModel.es,
                                        title_de: self.desLangModel.de,
                                        title_ko: self.desLangModel.ko,
                                        title_ru: self.desLangModel.ru,
                                        title_pt: self.desLangModel.pt
                                    }
                                }).success(function (json) {

                                    if (self.isNewPoster) {
                                        $.ajax({
                                            url: "api/boutique/uploadPhoto.php",
                                            method: "POST",
                                            dataType: "json",
                                            data: {image: App.imageData}
                                        }).success(function (json) {
                                            console.log(json);

                                            if (json.status == 502) {
                                                alert(App.strings.sessionTimeOut);
                                                location.reload();
                                                return;
                                            }

                                            self.posterFilePath = json.data.filepath;

                                            for (var x = 0; x < self.selected_language.length; x++) {
                                                if (x != self.selected_language.length - 1) {
                                                    selectedLanguageList += self.selected_language[x] + ",";
                                                }
                                                else {
                                                    selectedLanguageList += self.selected_language[x];
                                                }
                                            }

                                            for (var x = 0; x < self.selected_subtitle.length; x++) {
                                                if (x != self.selected_subtitle.length - 1) {
                                                    selectedSubtitleList += self.selected_subtitle[x] + ",";
                                                }
                                                else {
                                                    selectedSubtitleList += self.selected_subtitle[x];
                                                }
                                            }

                                            if ($("#stockInput").val() != self.stock) {

                                                var stockChanged = $("#stockInput").val() - self.stock;
                                                $.ajax({
                                                    url: "api/boutique/updateProductStock.php",
                                                    method: "POST",
                                                    dataType: "json",
                                                    data: {
                                                        movieTitleId: App.currentId,
                                                        descriptionId: App.currentItem.descriptionId,
                                                        genreId: self.selected_genre,
                                                        year: $("#yearInput").val(),
                                                        $posterurl: self.posterFilePath,
                                                        language: selectedLanguageList,
                                                        subtitle: selectedSubtitleList,
                                                        stockChanged: stockChanged
                                                    }
                                                }).success(function (json) {
                                                    if (json.status == 502) {
                                                        alert(App.strings.sessionTimeOut);
                                                        location.reload();
                                                        return;
                                                    }

                                                    $.ajax({
                                                        url: "api/boutique/updateProduct.php",
                                                        method: "POST",
                                                        dataType: "json",
                                                        data: {
                                                            movieTitleId: App.currentId,
                                                            genreId: self.selected_genre,
                                                            year: $("#yearInput").val(),
                                                            posterurl: self.posterFilePath,
                                                            language: selectedLanguageList,
                                                            subtitle: selectedSubtitleList,
                                                        }
                                                    }).success(function (json) {
                                                        console.log(json);
                                                        App.goUpper2Level();
                                                    }).error(function (d) {
                                                        console.log('error');
                                                        console.log(d);
                                                    });
                                                }).error(function (d) {
                                                    console.log('error');
                                                    console.log(d);
                                                });

                                            } else {
                                                $.ajax({
                                                    url: "api/boutique/updateProduct.php",
                                                    method: "POST",
                                                    dataType: "json",
                                                    data: {
                                                        movieTitleId: App.currentId,
                                                        genreId: self.selected_genre,
                                                        year: $("#yearInput").val(),
                                                        posterurl: self.posterFilePath,
                                                        language: selectedLanguageList,
                                                        subtitle: selectedSubtitleList,
                                                    }
                                                }).success(function (json) {
                                                    console.log(json);
                                                    App.goUpper2Level();
                                                }).error(function (d) {
                                                    console.log('error');
                                                    console.log(d);
                                                });
                                            }


                                        }).error(function (d) {
                                            console.log('error');
                                            console.log(d);
                                            alert("Upload photo error, please try again.");
                                            self.destroy();
                                        });
                                    } else {
                                        //updateItem Description of Item

                                        for (var x = 0; x < self.selected_language.length; x++) {
                                            if (x != self.selected_language.length - 1) {
                                                selectedLanguageList += self.selected_language[x] + ",";
                                            }
                                            else {
                                                selectedLanguageList += self.selected_language[x];
                                            }
                                        }

                                        for (var x = 0; x < self.selected_subtitle.length; x++) {
                                            if (x != self.selected_subtitle.length - 1) {
                                                selectedSubtitleList += self.selected_subtitle[x] + ",";
                                            }
                                            else {
                                                selectedSubtitleList += self.selected_subtitle[x];
                                            }
                                        }

                                        if ($("#stockInput").val() != self.stock) {

                                            var stockChanged = $("#stockInput").val() - self.stock;


                                            $.ajax({
                                                url: "api/boutique/updateProductStock.php",
                                                method: "POST",
                                                dataType: "json",
                                                data: {
                                                    movieTitleId: App.currentId,
                                                    descriptionId: App.currentItem.descriptionId,
                                                    genreId: self.selected_genre,
                                                    year: $("#yearInput").val(),
                                                    posterurl: self.posterFilePath,
                                                    language: selectedLanguageList,
                                                    subtitle: selectedSubtitleList,
                                                    stockChanged: stockChanged
                                                }
                                            }).success(function (json) {
                                                if (json.status == 502) {
                                                    alert(App.strings.sessionTimeOut);
                                                    location.reload();
                                                    return;
                                                }

                                                $.ajax({
                                                    url: "api/boutique/updateProduct.php",
                                                    method: "POST",
                                                    dataType: "json",
                                                    data: {
                                                        movieTitleId: App.currentId,
                                                        genreId: self.selected_genre,
                                                        year: $("#yearInput").val(),
                                                        posterurl: self.posterFilePath,
                                                        language: selectedLanguageList,
                                                        subtitle: selectedSubtitleList,
                                                    }
                                                }).success(function (json) {
                                                    console.log(json);
                                                    App.goUpper2Level();
                                                }).error(function (d) {
                                                    console.log('error');
                                                    console.log(d);
                                                });
                                            }).error(function (d) {
                                                console.log('error');
                                                console.log(d);
                                            });

                                        } else {
                                            $.ajax({
                                                url: "api/boutique/updateProduct.php",
                                                method: "POST",
                                                dataType: "json",
                                                data: {
                                                    movieTitleId: App.currentId,
                                                    genreId: self.selected_genre,
                                                    year: $("#yearInput").val(),
                                                    posterurl: self.posterFilePath,
                                                    language: selectedLanguageList,
                                                    subtitle: selectedSubtitleList,
                                                }
                                            }).success(function (json) {
                                                App.goUpper2Level();
                                            }).error(function (d) {
                                                console.log('error');
                                                console.log(d);
                                            });
                                        }
                                    }


                                }).error(function (d) {
                                    console.log('error');
                                    console.log(d);
                                });

                            }).error(function (d) {
                                console.log('error');
                                console.log(d);
                            });
                            App.hideLoading();

                        }
                        //add mew message
                        else {

                            var selectedLanguageList = '';
                            var selectedSubtitleList = '';

                            $.ajax({
                                url: "api/boutique/uploadPhoto.php",
                                method: "POST",
                                dataType: "json",
                                data: {image: App.imageData}
                            }).success(function (json) {
                                console.log(json);

                                if (json.status == 502) {
                                    alert(App.strings.sessionTimeOut);
                                    location.reload();
                                    return;
                                }

                                self.posterFilePath = json.data.filepath;

                                for (var x = 0; x < self.selected_language.length; x++) {
                                    if (x != self.selected_language.length - 1) {
                                        selectedLanguageList += self.selected_language[x] + ",";
                                    }
                                    else {
                                        selectedLanguageList += self.selected_language[x];
                                    }
                                }

                                for (var x = 0; x < self.selected_subtitle.length; x++) {
                                    if (x != self.selected_subtitle.length - 1) {
                                        selectedSubtitleList += self.selected_subtitle[x] + ",";
                                    }
                                    else {
                                        selectedSubtitleList += self.selected_subtitle[x];
                                    }
                                }

                                $.ajax({
                                    url: "api/boutique/addProduct.php",
                                    method: "POST",
                                    dataType: "json",
                                    data: {
                                        genreId: self.selected_genre,
                                        statusId: 0,
                                        language: selectedLanguageList,
                                        subtitle: selectedSubtitleList,
                                        year: $("#yearInput").val(),
                                        poster: self.posterFilePath,
                                        stock: $("#stockInput").val(),

                                        title_en: $('#detailTitle_en').val(),
                                        title_zh_hk: self.titleLangModel.zh_hk,
                                        title_zh_cn: self.titleLangModel.zh_cn,
                                        title_jp: self.titleLangModel.jp,
                                        title_fr: self.titleLangModel.fr,
                                        title_ar: self.titleLangModel.ar,
                                        title_es: self.titleLangModel.es,
                                        title_de: self.titleLangModel.de,
                                        title_ko: self.titleLangModel.ko,
                                        title_ru: self.titleLangModel.ru,
                                        title_pt: self.titleLangModel.pt,

                                        description_en: editor_en.getData(),
                                        description_zh_hk: self.desLangModel.zh_hk,
                                        description_zh_cn: self.desLangModel.zh_cn,
                                        description_jp: self.desLangModel.jp,
                                        description_fr: self.desLangModel.fr,
                                        description_ar: self.desLangModel.ar,
                                        description_es: self.desLangModel.es,
                                        description_de: self.desLangModel.de,
                                        description_ko: self.desLangModel.ko,
                                        description_ru: self.desLangModel.ru,
                                        description_pt: self.desLangModel.pt
                                    }
                                }).success(function (json) {
                                    if (json.status == 502) {
                                        alert(App.strings.sessionTimeOut);
                                        location.reload();
                                        return;
                                    }
                                    console.log(json);
                                    App.goUpperLevel();
                                }).error(function (d) {
                                    console.log('error123');
                                    console.log(d);
                                });

                            }).error(function (d) {
                                console.log('error345');
                                console.log(d);
                                alert("Upload photo error, please try again.");
                                App.goUpperLevel();
                            });
                        }
                        App.yesNoPopup.destroy();
                    },
                    msg: "Are you sure to add/edit product?"
                }
            );


        });

        //handle language select box
        $("#lang_selectionBox").change(function () {

            var editor_lang = CKEDITOR.instances.editor2;

            if ($("#lang_selectionBox").val() != "") {
                self.currentLang = $("#lang_selectionBox").val();
            }

            //setup enable/disable the other language input box
            if ($("#lang_selectionBox").val() == 'en') {
                $('#container_lang input').attr('disabled', 'disabled');
                editor_lang.setReadOnly(true);
            }
            else {
                //alert("enable");
                $('#container_lang input').removeAttr('disabled');
                editor_lang.setReadOnly(false);
            }

            $('#container_lang input').val(eval("self.titleLangModel." + $("#lang_selectionBox").val()));

            var string = "";
            if (self.desLangModel.en.indexOf("<br />") == -1) {
                string = eval("self.desLangModel." + $("#lang_selectionBox").val()).replace(/\n/g, "<br />");
                //string  = string.replace(/\r/g, "<br />");
            }
            else {
                string = eval("self.desLangModel." + $("#lang_selectionBox").val());
            }
            //string = string.replace(/\r/g, "<br />");

            editor_lang.setData(string);
        });

        $('#container_lang input').blur(function () {
            var editor_lang = CKEDITOR.instances.editor2;
            //alert("hi");
            eval("self.titleLangModel." + $("#lang_selectionBox").val() + "= $('#container_lang input').val()");

            console.log(self.titleLangModel);
        });

        $("#addImageBtn").on('click', function () {
            App.addPhotoPopup = new App.AddPhotoPopup(
                {
                    root: self
                }
            );
        });


        $("#takePictureField").on("change", self.gotPic);

    },

    getSelectedGerne: function (_self) {
        var self = _self;
        self.selected_genre = App.currentItem.genreId;
    },

    getSelectedLanguage: function (_self) {
        var self = _self;
        var selected_language = App.currentItem.lang;
        if (selected_language != null && selected_language.length > 0) {
            self.selected_language = selected_language.split(',');
        } else {
            self.selected_language[0] = null;
        }
    },

    getSelectedSubtitle: function (_self) {
        var self = _self;
        var selected_subtitle = App.currentItem.subtitle;
        if (selected_subtitle != null && selected_subtitle.length > 0) {
            self.selected_subtitle = selected_subtitle.split(',');
        } else {
            self.selected_subtitle[0] = null;
        }
    },

    initGenreList: function (_self) {
        var self = _self;

        $("#genre_list").empty();

        if (self.selected_genre == null) {
            self.selected_genre = self.genreList[0].id;
        }

        var str = "<select class='genre_select_button' >";
        for (var x = 0; x < self.genreList.length; x++) {
            if (self.genreList[x].id == self.selected_genre) {
                str = str + "<option selected=\"selected\" value='" + self.genreList[x].id + "'>" + self.genreList[x].en + "</option>";
            } else {
                str = str + "<option value='" + self.genreList[x].id + "'>" + self.genreList[x].en + "</option>";
            }
        }

        str = str + "</select>";

        $("#genre_list").html(str);

        $(".genre_select_button").on('change', function () {
            console.log("genre onchange : " + this.value);
            self.selected_genre = this.value;
        });
    },

    initLanguageList: function (_self) {
        var self = _self;

        //clear up first
        $("#language_list1").empty();
        $("#language_list2").empty();
        $("#language_list3").empty();

        var str = "<select id='language1_select'>";
        str = str + "<option value='" + -1 + "'>" + "--" + "</option>";
        for (var x = 0; x < self.languageList.length; x++) {
            if (self.selected_language != null && self.selected_language[0] != null && self.selected_language[0] == self.languageList[x].id) {
                str = str + "<option selected=\"selected\" value='" + self.languageList[x].id + "'>" + self.languageList[x].en + "</option>";
            } else {
                str = str + "<option value='" + self.languageList[x].id + "'>" + self.languageList[x].en + "</option>";
            }
        }
        str = str + "</select>";
        $("#language_list_1").html(str);

        var str = "<select id='language2_select'>";
        str = str + "<option value='" + -1 + "'>" + "--" + "</option>";
        for (var x = 0; x < self.languageList.length; x++) {
            if (self.selected_language != null && self.selected_language[1] != null && self.selected_language[1] == self.languageList[x].id) {
                str = str + "<option selected=\"selected\" value='" + self.languageList[x].id + "'>" + self.languageList[x].en + "</option>";
            } else {
                str = str + "<option value='" + self.languageList[x].id + "'>" + self.languageList[x].en + "</option>";
            }
        }
        str = str + "</select>";
        $("#language_list_2").html(str);

        var str = "<select id='language3_select'>";
        str = str + "<option value='" + -1 + "'>" + "--" + "</option>";
        for (var x = 0; x < self.languageList.length; x++) {
            if (self.selected_language != null && self.selected_language[2] != null && self.selected_language[2] == self.languageList[x].id) {
                str = str + "<option selected=\"selected\" value='" + self.languageList[x].id + "'>" + self.languageList[x].en + "</option>";
            } else {
                str = str + "<option value='" + self.languageList[x].id + "'>" + self.languageList[x].en + "</option>";
            }
        }
        str = str + "</select>";
        $("#language_list_3").html(str);

        $("#language1_select").on('change', function () {
            var value = this.options[this.selectedIndex].value;
            console.log("language1 onchange : " + value);
            if (this.value != -1) {
                self.selected_language[0] = value;
            } else {
                self.selected_language[0] = null;
            }
        });

        $("#language2_select").on('change', function () {
            var value = this.options[this.selectedIndex].value;
            console.log("language2 onchange : " + value);
            if (this.value != -1) {
                self.selected_language[1] = value;
            } else {
                self.selected_language[1] = null;
            }
        });

        $("#language3_select").on('change', function () {
            var value = this.options[this.selectedIndex].value;
            console.log("language3 onchange : " + value);
            if (this.value != -1) {
                self.selected_language[2] = value;
            } else {
                self.selected_language[2] = null;
            }
        });
    },


    initSubtitleList: function (_self) {
        var self = _self;

        //clear up first
        $("#subtitle_list_1").empty();
        $("#subtitle_list_2").empty();
        $("#subtitle_list_3").empty();

        var str = "<select id='subtitle1_select'>";
        str = str + "<option value='" + -1 + "'>" + "--" + "</option>";
        for (var x = 0; x < self.subtitleList.length; x++) {
            if (self.selected_subtitle != null && self.selected_subtitle[0] != null && self.selected_subtitle[0] == self.subtitleList[x].id) {
                str = str + "<option selected=\"selected\" value='" + self.subtitleList[x].id + "'>" + self.subtitleList[x].en + "</option>";
            } else {
                str = str + "<option value='" + self.subtitleList[x].id + "'>" + self.subtitleList[x].en + "</option>";
            }
        }
        str = str + "</select>";
        $("#subtitle_list_1").html(str);

        var str = "<select id='subtitle2_select'>";
        str = str + "<option value='" + -1 + "'>" + "--" + "</option>";
        for (var x = 0; x < self.subtitleList.length; x++) {
            if (self.selected_subtitle != null && self.selected_subtitle[1] != null && self.selected_subtitle[1] == self.subtitleList[x].id) {
                str = str + "<option selected=\"selected\" value='" + self.subtitleList[x].id + "'>" + self.subtitleList[x].en + "</option>";
            } else {
                str = str + "<option value='" + self.subtitleList[x].id + "'>" + self.subtitleList[x].en + "</option>";
            }
        }
        str = str + "</select>";
        $("#subtitle_list_2").html(str);

        var str = "<select id='subtitle3_select'>";
        str = str + "<option value='" + -1 + "'>" + "--" + "</option>";
        for (var x = 0; x < self.subtitleList.length; x++) {
            if (self.selected_subtitle != null && self.selected_subtitle[2] != null && self.selected_subtitle[2] == self.subtitleList[x].id) {
                str = str + "<option selected=\"selected\" value='" + self.subtitleList[x].id + "'>" + self.subtitleList[x].en + "</option>";
            } else {

                str = str + "<option value='" + self.subtitleList[x].id + "'>" + self.subtitleList[x].en + "</option>";
            }
        }
        str = str + "</select>";
        $("#subtitle_list_3").html(str);

        $("#subtitle1_select").on('change', function () {
            var value = this.options[this.selectedIndex].value;
            console.log("subtitle1 onchange : " + value);
            if (this.value != -1) {
                self.selected_subtitle[0] = value;
            } else {
                self.selected_subtitle[0] = null;
            }
        });

        $("#subtitle2_select").on('change', function () {
            var value = this.options[this.selectedIndex].value;
            console.log("subtitle2 onchange : " + value);
            if (this.value != -1) {
                self.selected_subtitle[1] = value;
            } else {
                self.selected_subtitle[1] = null;
            }
        });

        $("#subtitle3_select").on('change', function () {
            var value = this.options[this.selectedIndex].value;
            console.log("subtitle3 onchange : " + value);
            if (this.value != -1) {
                self.selected_subtitle[2] = value;
            } else {
                self.selected_subtitle[2] = null;
            }
        });
    },

    saveDescriptionLang: function () {
        var editor_lang = CKEDITOR.instances.editor2;
        eval("this.desLangModel." + $("#lang_selectionBox").val() + "= editor_lang.getData()");
    },
    close: function () {
        console.log("close fire");
    },
    destroy: function () {

        //COMPLETELY UNBIND THE VIEW

        this.selectGuestModel = null;
        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    gotPic: function (event) {

        var self = this;
        if (event.target.files.length == 1 && event.target.files[0].type.indexOf("image/") == 0) {

            self.isNewPoster = true;

            //get only filename without extension
            var file = event.target.files[0];
            var tempFileComponents = file.type.split("/");
            var fileExtension = tempFileComponents[1];
            App.imageFileName = file.name.substring(0, file.name.length - fileExtension.length - 1);
            App.imageFileName = App.imageFileName.replace(/\s+/g, '');

            console.log('File size =' + file.size);
            console.log('max size =' + self.uploadImageSizeLimit * 1024 * 1024);
            //image file exceed upper limited size, not allow to pass
            if (file.size > self.uploadImageSizeLimit * 1024 * 1024) {
                $("#takePictureField").val('');
                alert("File size is too big. Please upload image less than " + self.uploadImageSizeLimit + "mb.");
                return;
            }

            var url = window.URL ? window.URL : window.webkitURL;

            $('#uploadImage').empty();
            $('#uploadImage').attr('src', url.createObjectURL(event.target.files[0]));
            var canvasImage = document.createElement("img");
            canvasImage.src = url.createObjectURL(event.target.files[0]);

            $("#imageContainer").show();

            canvasImage.onload = function () {
                console.log("Image onLoad fire");
                //change image data to base64
                var canvas = document.createElement('CANVAS'),
                    ctx = canvas.getContext('2d'),
                    outputFormat = "image/jpeg";

                canvas.height = canvasImage.height;
                canvas.width = canvasImage.width;
                ctx.drawImage(canvasImage, 0, 0);
                App.imageData = canvas.toDataURL(outputFormat);
                //alert("App.imageData = " + App.imageData);
                App.imageData = App.imageData.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");

                canvas = null;
            };
        }
        else {
            alert("Please select an image file.");
            $('#uploadImage').val("");
        }
    },

    loadPhoto: function () {
        var self = this;
        if (self.posterFilePath != null && !self.posterFilePath.isEmpty) {
            var srcPath = "http://localhost/" + App.baseFolder + "/" + self.posterFilePath;
            $('#uploadImage').empty();
            $('#uploadImage').attr('src', srcPath);
            $("#imageContainer").show();
        }
    },

    loadStock: function () {
        var self = this;
        if (self.stock != null) {
            stockInput.value = self.stock;
        }
    },

    loadYear: function () {
        var self = this;
        if (self.year != null) {
            yearInput.value = self.year;
        }
    },

    isHide: false
});
