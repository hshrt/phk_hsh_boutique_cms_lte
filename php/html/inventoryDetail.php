<div class='box detail_container'>
    <div class="box-header" style="padding-top:0px">
        <div style='width:100%;height:auto;float:left'>
            <h1 id='itemName'></h1>
            <select id='lang_selectionBox' name='language'>

                <option value='en'>English</option>
                <option value='zh_cn'>Simplied Chinese</option>
                <option value='fr'>French</option>
                <option value='jp'>Japanese</option>
                <option value='ar'>Arabic</option>
                <option value='es'>Spanish</option>
                <option value='de'>German</option>
                <option value='ko'>Korean</option>
                <option value='ru'>Russian</option>
                <option value='pt'>Portuguese</option>
                <option value='zh_hk'>Traditional Chinese</option>

            </select>
        </div>
    </div>

    <div class="box-body">
        <div class='half_width_container form-group' id='container_en'>
            <label for='title' id='titleLabel_en'>Title : </label>
            <input id='detailTitle_en' class='form-control' type='text' name='title' tabindex='1' data-type='text'
                   value='' autofocus=''>
        </div>
        <div class='half_width_container form-group' id='container_lang'>
            <label for='title' id='titleLabel_lang'>Title : </label>
            <input id='detailTitle_lang' class='form-control' type='text' name='title' tabindex='1' data-type='text'
                   value='' autofocus='' disabled>
        </div>


        <div class='half_width_container form-group' id='textEditor_en'>
            <label id='des_en'>
                Description :
            </label>
            <div id="textareaContainer">
                <input cols="80" id="editor1" name="editor1" rows="10">
                </input>
            </div>
        </div>
        <div class='half_width_container form-group' id='textEditor_lang'>
            <label id='des_lang'>
                Description :
            </label>
            <div id="textareaContainer">
            <textarea cols="80" id="editor2" name="editor2" rows="10">
            </textarea>
            </div>
        </div>

        <div class='half_width_container form-group' id='container_checkIn'>
            <div class='half_width_container_small' id='container_checkInDate'>
                <label for='title' id='yearLabel'>Year : </label>
                <input id='yearInput' type='text' name='title' value='' autofocus=''>
            </div>
        </div>

        <div class='full_width_container form-group'>
            <div class='full_width_container_small' id='container_checkInDate'>
                <label for='title' id='genreLabel'>Genre : </label>
                <div style='width:auto'>
                    <div id="genre_list">
                    </div>
                </div>
            </div>

        </div>

        <hr>

        <div class='full_width_container form-group'>
            <div class='full_width_container_small'>
                <label for='title' id='languageLabel'>Language : </label>
                <div style='width:auto'>
                    <table>
                        <tr>
                            <td>
                                <div style="padding:10px 10px;" id="language_list_1"/>
                            </td>
                            <td>
                                <div style="padding:10px 10px;" id="language_list_2"/>
                            </td>
                            <td>
                                <div style="padding:10px 10px;" id="language_list_3"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>

        <hr>

        <div class='full_width_container form-group'>
            <div class='full_width_container_small'>
                <label for='title' id='subtitleLabel'>Subtitle : </label>
                <div style='width:auto'>
                    <table>
                        <tr>
                            <td>
                                <div style="padding:10px 10px;" id="subtitle_list_1"/>
                            </td>
                            <td>
                                <div style="padding:10px 10px;" id="subtitle_list_2"/>
                            </td>
                            <td>
                                <div style="padding:10px 10px;" id="subtitle_list_3"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>

        <hr>

        <div class='half_width_container form-group' id='container_checkIn'>
            <div class='half_width_container_small' id='container_checkInDate'>
                <label for='title' id='stockLabel'>Total Stock : </label>
                <input id='stockInput' type='number' name='stock' min="1" value='1' autofocus=''>
            </div>
        </div>

        <div class='full_width_container form-group' id='textEditor_lang'>
            <div class='full_width_container_small' id='container_checkInDate'>
                <label for='title' id='posterLabel'>Poster : </label>

            </div>
            <div id="middle">
                <!--upload media UI-->
                <input id="takePictureField" type="file" accept="image/*">
                <div id="imageContainer">
                    <img style="width: 240px; height: 360px" id="uploadImage"/>
                </div>
            </div>
        </div>

        <div class='save_cancel_container'>
            <div class='round_btn btn bg-maroon' id='delete_btn'>Delete Boutique</div>
            <div class='round_btn btn bg-olive' id='save_btn'>Save Boutique</div>
        </div>
    </div>
</div>