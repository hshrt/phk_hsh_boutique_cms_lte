<?php

$type = '';
$title = '';

if(isset($_GET["type"])) {
    $type = $_GET["type"];

    if($type == "genre") {
        $title = "Genre";
    } else if($type == "language"){
        $title = "Language";
    } else if ($type == "subtitle"){
        $title = "Subtitle";
    }  else if ($type == "config"){
        $title = "Config";
    } else if ($type == "division"){
        $title = "Division";
    } else if ($type == "delete"){
        $title = "Delete";
    }
}

if ($type == "config"){

    echo "<div id=\"listViewContainer\">
        <div class=\"box\" id=\"list_boxer\">
            <div class=\"box-header\" style=\"padding-top:0px\">
                <h3 style=\"float:left\" id=\"list_head_title\">".$title."</h3>
            </div>
    
            <div class=\"box-body\">
                <div id=\"headerContainer\">
                    <div id=\"scTitle\" class=\"tableHead\">Key</div>
                    <div id=\"engTitle\" class=\"tableHead\">Value</div>
                </div>
    
                <div id=\"itemContainer\">
    
                </div>
                <div id=\"noResultMsg\">There are no ".$title."</div>
                <div id='paginationContainer'>
    
                </div>
            </div>
    
            <div class=\"box-footer\">
    
            </div>
        </div>
    </div>";

} else if ($type == "delete") {

    echo "<div id=\"listViewContainer\">
        <div class=\"box\" id=\"list_boxer\">
            <div class=\"box-header\" style=\"padding-top:0px\">
                <h3 style=\"float:left\" id=\"list_head_title\">".$title."</h3>
            </div>
    
            <div class=\"box-body\">
    
                <div id=\"itemContainer\">
    
                </div>
                <div id=\"noResultMsg\">There are no ".$title."</div>
                <div id='paginationContainer'>
    
                </div>
            </div>
    
            <div class=\"box-footer\">
    
            </div>
        </div>
    </div>";
	
} else {

    echo "<div id=\"listViewContainer\">
        <div class=\"box\" id=\"list_boxer\">
            <div class=\"box-header\" style=\"padding-top:0px\">
                <h3 style=\"float:left\" id=\"list_head_title\">".$title."</h3>
                <div class='submit round_btn btn bg-olive' id='addBtn'>+Add ".$title."</div>
    
                <input id=\"search\" class=\"form-control\" type='text' name='title' tabindex='1' data-type='text' placeholder=\"Search...\" autofocus=''>
            </div>
    
            <div class=\"box-body\">
                <div id=\"headerContainer\">
                    <div id=\"scTitle\" class=\"tableHead\">Simplified Chinese</div>
                    <div id=\"engTitle\" class=\"tableHead\">English</div>
                </div>
    
                <div id=\"itemContainer\">
    
                </div>
                <div id=\"noResultMsg\">There are no ".$title."</div>
                <div id='paginationContainer'>
    
                </div>
            </div>
    
            <div class=\"box-footer\">
    
            </div>
        </div>
    </div>";
}
?>