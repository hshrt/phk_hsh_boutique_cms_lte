<div class="box detail_container" id=''>
    <div class="box-body">
            <div style='display: inline-block;width:100%;'>
                <div style='width:25%;float:left;font-weight:bold'>Day  </div>
				<div style='width:14%;float:left;font-weight:bold'>Open/Close?  </div>
                <div style='width:17%;float:left;font-weight:bold'>Start Time</div>
                <div style='width:25%;float:left;font-weight:bold'>End Time</div>
              
            </div>
			<br><br>
            <div style='display: inline-block;width:100%;'>
               <div style='width:25%;float:left'>Sunday  </div>
               <select style='width:5%;height:20px;margin-right:9%;' id='statusSunday' name='status'>
                    <option value='open'>open</option>
                    <option value='close'>close</option>
                </select>			   
               <select style='width:5%;height:20px;margin-right:0.5%;' id='startHourSunday' name='status'>
                    <option value='00'>00</option>
                    <option value='01'>01</option>
					<option value='02'>02</option>
                    <option value='03'>03</option>
                    <option value='04'>04</option>
                    <option value='05'>05</option>
                    <option value='06'>06</option>
					<option value='07'>07</option>
					<option value='08'>08</option>
					<option value='09'>09</option>
					<option value='10'>10</option>
					<option value='11'>11</option>
					<option value='12'>12</option>
					<option value='13'>13</option>
					<option value='14'>14</option>
					<option value='15'>15</option>
					<option value='16'>16</option>
					<option value='17'>17</option>
					<option value='18'>18</option>
					<option value='19'>19</option>
					<option value='20'>20</option>
					<option value='21'>21</option>
					<option value='22'>22</option>
					<option value='23'>23</option>
                </select>
               <select style='width:5%;height:20px;margin-right:6%;' id='startMinSunday' name='status'>
                    <option value='00'>00</option>
                    <option value='10'>10</option>
					<option value='20'>20</option>
                    <option value='30'>30</option>
                    <option value='40'>40</option>
                    <option value='50'>50</option>
                </select>
               <select style='width:5%;height:20px;margin-right:0.5%;' id='endHourSunday' name='status'>
                    <option value='00'>00</option>
                    <option value='01'>01</option>
					<option value='02'>02</option>
                    <option value='03'>03</option>
                    <option value='04'>04</option>
                    <option value='05'>05</option>
                    <option value='06'>06</option>
					<option value='07'>07</option>
					<option value='08'>08</option>
					<option value='09'>09</option>
					<option value='10'>10</option>
					<option value='11'>11</option>
					<option value='12'>12</option>
					<option value='13'>13</option>
					<option value='14'>14</option>
					<option value='15'>15</option>
					<option value='16'>16</option>
					<option value='17'>17</option>
					<option value='18'>18</option>
					<option value='19'>19</option>
					<option value='20'>20</option>
					<option value='21'>21</option>
					<option value='22'>22</option>
					<option value='23'>23</option>
                </select>
               <select style='width:5%;height:20px;margin-right:5%;' id='endMinSunday' name='status'>
                    <option value='00'>00</option>
                    <option value='10'>10</option>
					<option value='20'>20</option>
                    <option value='30'>30</option>
                    <option value='40'>40</option>
                    <option value='50'>50</option>
                </select>
            </div>
			<br><br>
            <div style='display: inline-block;width:100%;'>
               <div style='width:25%;float:left'>Monday  </div>
               <select style='width:5%;height:20px;margin-right:9%;' id='statusMonday' name='status'>
                    <option value='open'>open</option>
                    <option value='close'>close</option>
                </select>			   
               <select style='width:5%;height:20px;margin-right:0.5%;' id='startHourMonday' name='status'>
                    <option value='00'>00</option>
                    <option value='01'>01</option>
					<option value='02'>02</option>
                    <option value='03'>03</option>
                    <option value='04'>04</option>
                    <option value='05'>05</option>
                    <option value='06'>06</option>
					<option value='07'>07</option>
					<option value='08'>08</option>
					<option value='09'>09</option>
					<option value='10'>10</option>
					<option value='11'>11</option>
					<option value='12'>12</option>
					<option value='13'>13</option>
					<option value='14'>14</option>
					<option value='15'>15</option>
					<option value='16'>16</option>
					<option value='17'>17</option>
					<option value='18'>18</option>
					<option value='19'>19</option>
					<option value='20'>20</option>
					<option value='21'>21</option>
					<option value='22'>22</option>
					<option value='23'>23</option>
                </select>
               <select style='width:5%;height:20px;margin-right:6%;' id='startMinMonday' name='status'>
                    <option value='00'>00</option>
                    <option value='10'>10</option>
					<option value='20'>20</option>
                    <option value='30'>30</option>
                    <option value='40'>40</option>
                    <option value='50'>50</option>
                </select>
               <select style='width:5%;height:20px;margin-right:0.5%;' id='endHourMonday' name='status'>
                    <option value='00'>00</option>
                    <option value='01'>01</option>
					<option value='02'>02</option>
                    <option value='03'>03</option>
                    <option value='04'>04</option>
                    <option value='05'>05</option>
                    <option value='06'>06</option>
					<option value='07'>07</option>
					<option value='08'>08</option>
					<option value='09'>09</option>
					<option value='10'>10</option>
					<option value='11'>11</option>
					<option value='12'>12</option>
					<option value='13'>13</option>
					<option value='14'>14</option>
					<option value='15'>15</option>
					<option value='16'>16</option>
					<option value='17'>17</option>
					<option value='18'>18</option>
					<option value='19'>19</option>
					<option value='20'>20</option>
					<option value='21'>21</option>
					<option value='22'>22</option>
					<option value='23'>23</option>
                </select>
               <select style='width:5%;height:20px;margin-right:5%;' id='endMinMonday' name='status'>
                    <option value='00'>00</option>
                    <option value='10'>10</option>
					<option value='20'>20</option>
                    <option value='30'>30</option>
                    <option value='40'>40</option>
                    <option value='50'>50</option>
                </select>
            </div>
			<br><br>
            <div style='display: inline-block;width:100%;'>
               <div style='width:25%;float:left'>Tuesday  </div>
               <select style='width:5%;height:20px;margin-right:9%;' id='statusTuesday' name='status'>
                    <option value='open'>open</option>
                    <option value='close'>close</option>
                </select>			   
               <select style='width:5%;height:20px;margin-right:0.5%;' id='startHourTuesday' name='status'>
                    <option value='00'>00</option>
                    <option value='01'>01</option>
					<option value='02'>02</option>
                    <option value='03'>03</option>
                    <option value='04'>04</option>
                    <option value='05'>05</option>
                    <option value='06'>06</option>
					<option value='07'>07</option>
					<option value='08'>08</option>
					<option value='09'>09</option>
					<option value='10'>10</option>
					<option value='11'>11</option>
					<option value='12'>12</option>
					<option value='13'>13</option>
					<option value='14'>14</option>
					<option value='15'>15</option>
					<option value='16'>16</option>
					<option value='17'>17</option>
					<option value='18'>18</option>
					<option value='19'>19</option>
					<option value='20'>20</option>
					<option value='21'>21</option>
					<option value='22'>22</option>
					<option value='23'>23</option>
                </select>
               <select style='width:5%;height:20px;margin-right:6%;' id='startMinTuesday' name='status'>
                    <option value='00'>00</option>
                    <option value='10'>10</option>
					<option value='20'>20</option>
                    <option value='30'>30</option>
                    <option value='40'>40</option>
                    <option value='50'>50</option>
                </select>
               <select style='width:5%;height:20px;margin-right:0.5%;' id='endHourTuesday' name='status'>
                    <option value='00'>00</option>
                    <option value='01'>01</option>
					<option value='02'>02</option>
                    <option value='03'>03</option>
                    <option value='04'>04</option>
                    <option value='05'>05</option>
                    <option value='06'>06</option>
					<option value='07'>07</option>
					<option value='08'>08</option>
					<option value='09'>09</option>
					<option value='10'>10</option>
					<option value='11'>11</option>
					<option value='12'>12</option>
					<option value='13'>13</option>
					<option value='14'>14</option>
					<option value='15'>15</option>
					<option value='16'>16</option>
					<option value='17'>17</option>
					<option value='18'>18</option>
					<option value='19'>19</option>
					<option value='20'>20</option>
					<option value='21'>21</option>
					<option value='22'>22</option>
					<option value='23'>23</option>
                </select>
               <select style='width:5%;height:20px;margin-right:5%;' id='endMinTuesday' name='status'>
                    <option value='00'>00</option>
                    <option value='10'>10</option>
					<option value='20'>20</option>
                    <option value='30'>30</option>
                    <option value='40'>40</option>
                    <option value='50'>50</option>
                </select>
            </div>
			<br><br>
            <div style='display: inline-block;width:100%;'>
               <div style='width:25%;float:left'>Wednesday </div>
               <select style='width:5%;height:20px;margin-right:9%;' id='statusWednesday' name='status'>
                    <option value='open'>open</option>
                    <option value='close'>close</option>
                </select>			   
               <select style='width:5%;height:20px;margin-right:0.5%;' id='startHourWednesday' name='status'>
                    <option value='00'>00</option>
                    <option value='01'>01</option>
					<option value='02'>02</option>
                    <option value='03'>03</option>
                    <option value='04'>04</option>
                    <option value='05'>05</option>
                    <option value='06'>06</option>
					<option value='07'>07</option>
					<option value='08'>08</option>
					<option value='09'>09</option>
					<option value='10'>10</option>
					<option value='11'>11</option>
					<option value='12'>12</option>
					<option value='13'>13</option>
					<option value='14'>14</option>
					<option value='15'>15</option>
					<option value='16'>16</option>
					<option value='17'>17</option>
					<option value='18'>18</option>
					<option value='19'>19</option>
					<option value='20'>20</option>
					<option value='21'>21</option>
					<option value='22'>22</option>
					<option value='23'>23</option>
                </select>
               <select style='width:5%;height:20px;margin-right:6%;' id='startMinWednesday' name='status'>
                    <option value='00'>00</option>
                    <option value='10'>10</option>
					<option value='20'>20</option>
                    <option value='30'>30</option>
                    <option value='40'>40</option>
                    <option value='50'>50</option>
                </select>
               <select style='width:5%;height:20px;margin-right:0.5%;' id='endHourWednesday' name='status'>
                    <option value='00'>00</option>
                    <option value='01'>01</option>
					<option value='02'>02</option>
                    <option value='03'>03</option>
                    <option value='04'>04</option>
                    <option value='05'>05</option>
                    <option value='06'>06</option>
					<option value='07'>07</option>
					<option value='08'>08</option>
					<option value='09'>09</option>
					<option value='10'>10</option>
					<option value='11'>11</option>
					<option value='12'>12</option>
					<option value='13'>13</option>
					<option value='14'>14</option>
					<option value='15'>15</option>
					<option value='16'>16</option>
					<option value='17'>17</option>
					<option value='18'>18</option>
					<option value='19'>19</option>
					<option value='20'>20</option>
					<option value='21'>21</option>
					<option value='22'>22</option>
					<option value='23'>23</option>
                </select>
               <select style='width:5%;height:20px;margin-right:5%;' id='endMinWednesday' name='status'>
                    <option value='00'>00</option>
                    <option value='10'>10</option>
					<option value='20'>20</option>
                    <option value='30'>30</option>
                    <option value='40'>40</option>
                    <option value='50'>50</option>
                </select>
            </div>
			<br><br>
            <div style='display: inline-block;width:100%;'>
               <div style='width:25%;float:left'>Thursday  </div>
               <select style='width:5%;height:20px;margin-right:9%;' id='statusThursday' name='status'>
                    <option value='open'>open</option>
                    <option value='close'>close</option>
                </select>			   
               <select style='width:5%;height:20px;margin-right:0.5%;' id='startHourThursday' name='status'>
                    <option value='00'>00</option>
                    <option value='01'>01</option>
					<option value='02'>02</option>
                    <option value='03'>03</option>
                    <option value='04'>04</option>
                    <option value='05'>05</option>
                    <option value='06'>06</option>
					<option value='07'>07</option>
					<option value='08'>08</option>
					<option value='09'>09</option>
					<option value='10'>10</option>
					<option value='11'>11</option>
					<option value='12'>12</option>
					<option value='13'>13</option>
					<option value='14'>14</option>
					<option value='15'>15</option>
					<option value='16'>16</option>
					<option value='17'>17</option>
					<option value='18'>18</option>
					<option value='19'>19</option>
					<option value='20'>20</option>
					<option value='21'>21</option>
					<option value='22'>22</option>
					<option value='23'>23</option>
                </select>
               <select style='width:5%;height:20px;margin-right:6%;' id='startMinThursday' name='status'>
                    <option value='00'>00</option>
                    <option value='10'>10</option>
					<option value='20'>20</option>
                    <option value='30'>30</option>
                    <option value='40'>40</option>
                    <option value='50'>50</option>
                </select>
               <select style='width:5%;height:20px;margin-right:0.5%;' id='endHourThursday' name='status'>
                    <option value='00'>00</option>
                    <option value='01'>01</option>
					<option value='02'>02</option>
                    <option value='03'>03</option>
                    <option value='04'>04</option>
                    <option value='05'>05</option>
                    <option value='06'>06</option>
					<option value='07'>07</option>
					<option value='08'>08</option>
					<option value='09'>09</option>
					<option value='10'>10</option>
					<option value='11'>11</option>
					<option value='12'>12</option>
					<option value='13'>13</option>
					<option value='14'>14</option>
					<option value='15'>15</option>
					<option value='16'>16</option>
					<option value='17'>17</option>
					<option value='18'>18</option>
					<option value='19'>19</option>
					<option value='20'>20</option>
					<option value='21'>21</option>
					<option value='22'>22</option>
					<option value='23'>23</option>
                </select>
               <select style='width:5%;height:20px;margin-right:5%;' id='endMinThursday' name='status'>
                    <option value='00'>00</option>
                    <option value='10'>10</option>
					<option value='20'>20</option>
                    <option value='30'>30</option>
                    <option value='40'>40</option>
                    <option value='50'>50</option>
                </select>
            </div>
			<br><br>
            <div style='display: inline-block;width:100%;'>
               <div style='width:25%;float:left'>Friday  </div>
               <select style='width:5%;height:20px;margin-right:9%;' id='statusFriday' name='status'>
                    <option value='open'>open</option>
                    <option value='close'>close</option>
                </select>			   
               <select style='width:5%;height:20px;margin-right:0.5%;' id='startHourFriday' name='status'>
                    <option value='00'>00</option>
                    <option value='01'>01</option>
					<option value='02'>02</option>
                    <option value='03'>03</option>
                    <option value='04'>04</option>
                    <option value='05'>05</option>
                    <option value='06'>06</option>
					<option value='07'>07</option>
					<option value='08'>08</option>
					<option value='09'>09</option>
					<option value='10'>10</option>
					<option value='11'>11</option>
					<option value='12'>12</option>
					<option value='13'>13</option>
					<option value='14'>14</option>
					<option value='15'>15</option>
					<option value='16'>16</option>
					<option value='17'>17</option>
					<option value='18'>18</option>
					<option value='19'>19</option>
					<option value='20'>20</option>
					<option value='21'>21</option>
					<option value='22'>22</option>
					<option value='23'>23</option>
                </select>
               <select style='width:5%;height:20px;margin-right:6%;' id='startMinFriday' name='status'>
                    <option value='00'>00</option>
                    <option value='10'>10</option>
					<option value='20'>20</option>
                    <option value='30'>30</option>
                    <option value='40'>40</option>
                    <option value='50'>50</option>
                </select>
               <select style='width:5%;height:20px;margin-right:0.5%;' id='endHourFriday' name='status'>
                    <option value='00'>00</option>
                    <option value='01'>01</option>
					<option value='02'>02</option>
                    <option value='03'>03</option>
                    <option value='04'>04</option>
                    <option value='05'>05</option>
                    <option value='06'>06</option>
					<option value='07'>07</option>
					<option value='08'>08</option>
					<option value='09'>09</option>
					<option value='10'>10</option>
					<option value='11'>11</option>
					<option value='12'>12</option>
					<option value='13'>13</option>
					<option value='14'>14</option>
					<option value='15'>15</option>
					<option value='16'>16</option>
					<option value='17'>17</option>
					<option value='18'>18</option>
					<option value='19'>19</option>
					<option value='20'>20</option>
					<option value='21'>21</option>
					<option value='22'>22</option>
					<option value='23'>23</option>
                </select>
               <select style='width:5%;height:20px;margin-right:5%;' id='endMinFriday' name='status'>
                    <option value='00'>00</option>
                    <option value='10'>10</option>
					<option value='20'>20</option>
                    <option value='30'>30</option>
                    <option value='40'>40</option>
                    <option value='50'>50</option>
                </select>
            </div>
			<br><br>
            <div style='display: inline-block;width:100%;'>
               <div style='width:25%;float:left'>Saturday  </div>
               <select style='width:5%;height:20px;margin-right:9%;' id='statusSaturday' name='status'>
                    <option value='open'>open</option>
                    <option value='close'>close</option>
                </select>			   
               <select style='width:5%;height:20px;margin-right:0.5%;' id='startHourSaturday' name='status'>
                    <option value='00'>00</option>
                    <option value='01'>01</option>
					<option value='02'>02</option>
                    <option value='03'>03</option>
                    <option value='04'>04</option>
                    <option value='05'>05</option>
                    <option value='06'>06</option>
					<option value='07'>07</option>
					<option value='08'>08</option>
					<option value='09'>09</option>
					<option value='10'>10</option>
					<option value='11'>11</option>
					<option value='12'>12</option>
					<option value='13'>13</option>
					<option value='14'>14</option>
					<option value='15'>15</option>
					<option value='16'>16</option>
					<option value='17'>17</option>
					<option value='18'>18</option>
					<option value='19'>19</option>
					<option value='20'>20</option>
					<option value='21'>21</option>
					<option value='22'>22</option>
					<option value='23'>23</option>
                </select>
               <select style='width:5%;height:20px;margin-right:6%;' id='startMinSaturday' name='status'>
                    <option value='00'>00</option>
                    <option value='10'>10</option>
					<option value='20'>20</option>
                    <option value='30'>30</option>
                    <option value='40'>40</option>
                    <option value='50'>50</option>
                </select>
               <select style='width:5%;height:20px;margin-right:0.5%;' id='endHourSaturday' name='status'>
                    <option value='00'>00</option>
                    <option value='01'>01</option>
					<option value='02'>02</option>
                    <option value='03'>03</option>
                    <option value='04'>04</option>
                    <option value='05'>05</option>
                    <option value='06'>06</option>
					<option value='07'>07</option>
					<option value='08'>08</option>
					<option value='09'>09</option>
					<option value='10'>10</option>
					<option value='11'>11</option>
					<option value='12'>12</option>
					<option value='13'>13</option>
					<option value='14'>14</option>
					<option value='15'>15</option>
					<option value='16'>16</option>
					<option value='17'>17</option>
					<option value='18'>18</option>
					<option value='19'>19</option>
					<option value='20'>20</option>
					<option value='21'>21</option>
					<option value='22'>22</option>
					<option value='23'>23</option>
                </select>
               <select style='width:5%;height:20px;margin-right:5%;' id='endMinSaturday' name='status'>
                    <option value='00'>00</option>
                    <option value='10'>10</option>
					<option value='20'>20</option>
                    <option value='30'>30</option>
                    <option value='40'>40</option>
                    <option value='50'>50</option>
                </select>
            </div>
			<br><br>
            <div style='height:30px' class='search_round_btn btn bg-olive' id='submitBtn'>SUBMIT</div>
    </div>
</div>