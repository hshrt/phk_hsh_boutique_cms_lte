<!-- popup box setup -->
<div id="subTopBarContainer">
    <div id="ulContainer">
        <ul>
            <li class=""><span class="	glyphicon glyphicon-question-sign" id="requestLogo"></span>
                <a id="requestTab">Request Lists</a>
            </li>
            <li class=""><span class="glyphicon glyphicon-film" id="inventoryLogo"></span>
                <a id="inventoryTab">Boutique Inventory</a>
            </li>
            <li class=""><span class="glyphicon glyphicon-film" id="pendingLogo"></span>
                <a id="pendingTab">Pending Approvals</a>
            </li>			
            <li class=""><span class="glyphicon glyphicon-star-empty" id="latestLogo"></span>
                <a id="latestTab">Latest</a>
            </li>
            <li class=""><span class="glyphicon glyphicon-flash" id="genreLogo"></span>
                <a id="genreTab">Category</a>
            </li>
            <li class=""><span class="glyphicon glyphicon-send" id="notificationLogo"></span>
                <a id="notificationTab">Notification Channel</a>
            </li>
            <li class=""><span class="glyphicon glyphicon-cog" id="configLogo"></span>
                <a id="configTab">Config</a>
            </li>
            <li class=""><span class="glyphicon glyphicon-warning-sign" id="deleteLogo"></span>
                <a id="deleteTab">Delete</a>
            </li>
            <li class=""><span class="glyphicon glyphicon-time" id="openingHoursLogo"></span>
                <a id="openingHoursTab">Opening Hours</a>
            </li>			
        </ul>
    </div>
</div>
