<!-- popup box setup -->
<div class='popup_box_container'>
    <div class='popup_box'>
        <div id="head">

        </div>
        <div id="middle" style="height:300px;overflow:auto;">
            <table style="height: 50px;margin-bottom: 20px;" width="100%">
                <tbody id="stocklistcontainer">
                </tbody>
            </table>
        </div>

        <div id="new item">
            <input id="assetIdInput" class="popupNewAssetInput" width="30%"
                   placeholder="Enter quantity to add"></input></td>

            <span id="addNewInventoryBtn"
                  style='box-shadow: 2px 2px 7px 1px #013005; background-color:green; color: #fff; display: inline-block; padding: 8px 10px; font-weight: bold; border-radius: 7px;'>+ Add</span>
        </div>

        <div id="foot">
            <div class="actions">
                <div style='float:right' class='submit round_btn btn bg-olive' id="closeBtn">Close</div>
            </div>
        </div>
    </div>
</div>

