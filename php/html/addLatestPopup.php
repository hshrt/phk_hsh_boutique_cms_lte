<!-- popup box setup -->
<div class='popup_box_container'>
    <div class='popup_box'>
        <div id="head">

        </div>
        <div id="middle">

            <table>

                <tr id="tr_room">
                    <td class="borrowPopuptitle">Product to add:</td>
                    <td id="borrowMovie"><div id="movieSelection"></div></td>
                </tr>

            </table>

        </div>

        <div id="foot">
            <div class="actions">
                <div style='float:right; margin-left:20px'' class='submit round_btn btn bg-maroon' id="cancelBtn">Cancel</div>
                <div style='float:right' class='submit round_btn btn bg-olive' id="confirmBtn">Confirm</div>
            </div>
        </div>
    </div>
</div>

